package com.araged.movie.domian.use_case.get_movie_details

import com.araged.clean.Either
import com.araged.clean.UseCase
import com.araged.movie.domian.MovieRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject


/* */
class GetMovieDetailsUseCase @Inject constructor(
    private val movieRepository: MovieRepository,
    private val defaultDispatcher: CoroutineDispatcher = Dispatchers.IO
) : UseCase<GetMovieDetailsResponse, GetMovieDetailsParams, GetMovieDetailsFailure>() {

    
    override suspend fun run(
        params: GetMovieDetailsParams
    ): Either<GetMovieDetailsFailure, GetMovieDetailsResponse> = withContext(defaultDispatcher) {
        movieRepository.getMovieDetails(params.movieId)
    }

}
