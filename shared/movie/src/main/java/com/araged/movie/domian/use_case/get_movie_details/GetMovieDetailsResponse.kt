package com.araged.movie.domian.use_case.get_movie_details

import com.argaed.entities.movies.MovieDetails

data class GetMovieDetailsResponse(
    val movieDetails: MovieDetails
)
