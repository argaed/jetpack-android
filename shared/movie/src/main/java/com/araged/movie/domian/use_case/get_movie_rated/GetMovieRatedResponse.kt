package com.araged.movie.domian.use_case.get_movie_rated

import com.argaed.entities.movies.Movie


data class GetMovieRatedResponse(
    val movies: List<Movie>
)
