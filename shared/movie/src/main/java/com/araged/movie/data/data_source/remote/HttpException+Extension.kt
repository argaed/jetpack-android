package com.araged.movie.data.data_source.remote

import com.araged.httpclient.retrofit.HttpErrorCode
import com.araged.httpclient.retrofit.errorMessage
import com.araged.movie.domian.use_case.get_movie_details.GetMovieDetailsFailure
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularFailure
import com.araged.movie.domian.use_case.get_movie_rated.GetMovieRatedFailure
import com.araged.movie.domian.use_case.get_movie_upcoming.GetMovieUpcomingFailure
import retrofit2.HttpException


internal fun HttpException.toGetMoviePopularFailure(): GetMoviePopularFailure =
    when (HttpErrorCode.fromCode(code())) {
        HttpErrorCode.BAD_REQUEST -> GetMoviePopularFailure.UserDisabled
        else -> GetMoviePopularFailure.ServerFailure(code(), errorMessage())
    }


internal fun HttpException.toGetMovieRatedFailure(): GetMovieRatedFailure =
    when (HttpErrorCode.fromCode(code())) {
        HttpErrorCode.BAD_REQUEST -> GetMovieRatedFailure.UserDisabled
        else -> GetMovieRatedFailure.ServerFailure(code(), errorMessage())
    }


internal fun HttpException.toGetMovieUpcomingFailure(): GetMovieUpcomingFailure =
    when (HttpErrorCode.fromCode(code())) {
        HttpErrorCode.BAD_REQUEST -> GetMovieUpcomingFailure.UserDisabled
        else -> GetMovieUpcomingFailure.ServerFailure(code(), errorMessage())
    }


internal fun HttpException.toGetMovieDetailsFailure(): GetMovieDetailsFailure =
    when (HttpErrorCode.fromCode(code())) {
        HttpErrorCode.BAD_REQUEST -> GetMovieDetailsFailure.UserDisabled
        else -> GetMovieDetailsFailure.ServerFailure(code(), errorMessage())
    }

