package com.araged.movie.domian.use_case.get_movie_upcoming

import com.araged.clean.Either
import com.araged.clean.UseCase
import com.araged.movie.domian.MovieRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject


class GetMovieUpcomingUseCase @Inject constructor(
    private val movieRepository: MovieRepository,
    private val defaultDispatcher: CoroutineDispatcher = Dispatchers.IO
) : UseCase<GetMovieUpcomingResponse, GetMovieUpcomingParams, GetMovieUpcomingFailure>() {

    override suspend fun run(
        params: GetMovieUpcomingParams
    ): Either<GetMovieUpcomingFailure, GetMovieUpcomingResponse> = withContext(defaultDispatcher) {
        movieRepository.getMovieUpcoming()
    }

}