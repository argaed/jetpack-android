package com.araged.movie.data.data_source.remote

import com.araged.movie.data.data_source.remote.model.GetMovieDetailsHttpResponse
import com.araged.movie.data.data_source.remote.model.GetMovieHttpResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieApiServices {

    
    @GET(URL.GET_MOVIE_POPULAR)
    suspend fun getMoviePopular(): Response<GetMovieHttpResponse>

    
    @GET(URL.GET_MOVIE_TOP_RATED)
    suspend fun getMovieTopRated(): Response<GetMovieHttpResponse>

    
    @GET(URL.GET_MOVIE_UPCOMING)
    suspend fun getMovieUpcoming(): Response<GetMovieHttpResponse>

    
    @GET(URL.GET_MOVIE_DETAILS)
    suspend fun getMovieDetails(
        @Path("movie_id") movieId: Int,
        @Query("api_key") apiKey: String
    ): Response<GetMovieDetailsHttpResponse>

    
    private object URL {
        const val GET_MOVIE_POPULAR: String = "movie/popular?language=en-US&page=1"

        const val GET_MOVIE_TOP_RATED: String = "movie/top_rated?language=en-US&page=1"

        const val GET_MOVIE_UPCOMING: String = "movie/upcoming?language=en-US&page=1"

        const val GET_MOVIE_DETAILS: String = "movie/{movie_id}"

    }
}