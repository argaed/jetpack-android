package com.araged.movie.domian.use_case.get_movie_upcoming

import com.argaed.entities.movies.Movie


data class GetMovieUpcomingResponse(
    val movies: List<Movie>
)
