package com.araged.movie.domian.use_case.get_movie_rated

import com.araged.clean.Failure
import com.araged.failurehandler.HttpFailure
import com.araged.failurehandler.NetworkFailure


sealed class GetMovieRatedFailure : Failure() {

    
    object NetworkConnectionFailure : GetMovieRatedFailure(),  NetworkFailure

    
    object UserDisabled : GetMovieRatedFailure()

    
    object UserTimeOut : GetMovieRatedFailure()

    

    /**
     *
     */
    data class ServerFailure(
        override val code: Int,
        override val message: String
    ) : GetMovieRatedFailure(), HttpFailure

    
    data class UnknownFailure(
        val message: String
    ) : GetMovieRatedFailure()

}