package com.araged.movie.domian.use_case.get_movie_popular

import com.argaed.entities.movies.Movie


/* */
data class GetMoviePopularResponse(
    val movies: List<Movie>
)
