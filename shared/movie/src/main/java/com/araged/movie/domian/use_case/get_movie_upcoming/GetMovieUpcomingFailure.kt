package com.araged.movie.domian.use_case.get_movie_upcoming

import com.araged.clean.Failure
import com.araged.failurehandler.HttpFailure
import com.araged.failurehandler.NetworkFailure


/* */
sealed class GetMovieUpcomingFailure : Failure() {

    
    object NetworkConnectionFailure : GetMovieUpcomingFailure(), NetworkFailure

    
    object UserDisabled : GetMovieUpcomingFailure()

    
    data class ServerFailure(
        override val code: Int,
        override val message: String
    ) : GetMovieUpcomingFailure(), HttpFailure

    
    data class UnknownFailure(
        val message: String
    ) : GetMovieUpcomingFailure()

}