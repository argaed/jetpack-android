package com.araged.movie.domian.use_case.get_movie_rated

import com.araged.clean.Either
import com.araged.clean.UseCase
import com.araged.movie.domian.MovieRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject


class GetMovieRatedUseCase @Inject constructor(
    private val movieRepository: MovieRepository,
    private val defaultDispatcher: CoroutineDispatcher = Dispatchers.IO
) : UseCase<GetMovieRatedResponse, GetMovieRatedParams, GetMovieRatedFailure>() {

    override suspend fun run(
        params: GetMovieRatedParams
    ): Either<GetMovieRatedFailure, GetMovieRatedResponse> = withContext(defaultDispatcher){
        movieRepository.getMovieRated()
    }

}