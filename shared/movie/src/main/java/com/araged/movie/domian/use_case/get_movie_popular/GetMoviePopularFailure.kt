package com.araged.movie.domian.use_case.get_movie_popular

import com.araged.clean.Failure
import com.araged.failurehandler.HttpFailure
import com.araged.failurehandler.NetworkFailure


sealed class GetMoviePopularFailure : Failure() {

    
    object NetworkConnectionFailure : GetMoviePopularFailure(), NetworkFailure

    
    object UserDisabled : GetMoviePopularFailure()

    
    object UserTimeOut : GetMoviePopularFailure()

    

    /**
     *
     */
    data class ServerFailure(
        override val code: Int,
        override val message: String
    ) : GetMoviePopularFailure(), HttpFailure

    
    data class UnknownFailure(
        val message: String
    ) : GetMoviePopularFailure()

}