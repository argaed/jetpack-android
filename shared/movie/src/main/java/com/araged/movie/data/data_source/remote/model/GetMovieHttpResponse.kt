package com.araged.movie.data.data_source.remote.model


import com.araged.movie.data.data_source.remote.model.dto.MovieDto
import com.google.gson.annotations.SerializedName


data class GetMovieHttpResponse(
    @SerializedName("page") val page: Int,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("results") val results: List<MovieDto>,
    @SerializedName("total_results") val totalResults: Int
)