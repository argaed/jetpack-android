package com.araged.movie.data.data_source.remote.model.dto

import com.argaed.entities.movies.Movie
import com.google.gson.annotations.SerializedName


data class MovieDto(

    @SerializedName("overview") val overview: String,

    @SerializedName("original_language") val originalLanguage: String,

    @SerializedName("original_title") val originalTitle: String,

    @SerializedName("video") val video: Boolean,

    @SerializedName("title") val title: String,

    @SerializedName("poster_path") val posterPath: String,

    @SerializedName("backdrop_path") val backdropPath: String ?,

    @SerializedName("release_date") val releaseDate: String,

    @SerializedName("popularity") val popularity: Double,

    @SerializedName("vote_average") val voteAverage: Double,

    @SerializedName("id") val id: Int,

    @SerializedName("adult") val adult: Boolean,

    @SerializedName("vote_count") val voteCount: Int
){
    
    fun toMovie() = Movie(
        overview = overview,
        originalLanguage = originalLanguage,
        originalTitle = originalTitle,
        video = video,
        title = title,
        posterPath = posterPath,
        backdropPath = backdropPath,
        releaseDate = releaseDate,
        popularity = popularity,
        voteAverage = voteAverage,
        id = id,
        adult = adult,
        voteCount = voteCount
    )
}
