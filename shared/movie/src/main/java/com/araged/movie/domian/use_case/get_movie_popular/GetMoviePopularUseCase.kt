package com.araged.movie.domian.use_case.get_movie_popular

import com.araged.clean.Either
import com.araged.clean.UseCase
import com.araged.movie.domian.MovieRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject


class GetMoviePopularUseCase @Inject constructor(
    private val movieRepository: MovieRepository,
    private val defaultDispatcher: CoroutineDispatcher = Dispatchers.IO
) : UseCase<GetMoviePopularResponse, GetMoviePopularParams, GetMoviePopularFailure>() {

    override suspend fun run(
        params: GetMoviePopularParams
    ): Either<GetMoviePopularFailure, GetMoviePopularResponse> = withContext(defaultDispatcher) {
        movieRepository.getMoviePopular()
    }

}