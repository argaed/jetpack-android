package com.araged.movie.domian.use_case.get_movie_details

import com.araged.clean.Failure
import com.araged.failurehandler.HttpFailure
import com.araged.failurehandler.NetworkFailure


/* */
sealed class GetMovieDetailsFailure : Failure() {

    
    object NetworkConnectionFailure : GetMovieDetailsFailure(), NetworkFailure

    
    object UserDisabled : GetMovieDetailsFailure()

    
    object UserTimeOut : GetMovieDetailsFailure()

    /**
     *
     */
    data class ServerFailure(
        override val code: Int,
        override val message: String
    ) : GetMovieDetailsFailure(), HttpFailure

    
    data class UnknownFailure(
        val message: String
    ) : GetMovieDetailsFailure()

}