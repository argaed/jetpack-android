package com.araged.movie.data

import com.araged.clean.Either
import com.araged.movie.domian.use_case.get_movie_details.GetMovieDetailsFailure
import com.araged.movie.domian.use_case.get_movie_details.GetMovieDetailsResponse
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularFailure
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularResponse
import com.araged.movie.domian.use_case.get_movie_rated.GetMovieRatedFailure
import com.araged.movie.domian.use_case.get_movie_rated.GetMovieRatedResponse
import com.araged.movie.domian.use_case.get_movie_upcoming.GetMovieUpcomingFailure
import com.araged.movie.domian.use_case.get_movie_upcoming.GetMovieUpcomingResponse



interface MovieDataSource {

    
    suspend fun getMoviePopular(
    ): Either<GetMoviePopularFailure, GetMoviePopularResponse>

    
    suspend fun getMovieRated(
    ): Either<GetMovieRatedFailure, GetMovieRatedResponse>

    
    suspend fun getMovieUpcoming(
    ): Either<GetMovieUpcomingFailure, GetMovieUpcomingResponse>

    
    suspend fun getMovieDetails(
        movieID: Int
    ): Either<GetMovieDetailsFailure, GetMovieDetailsResponse>

}