package com.araged.movie.data.data_source.local

import com.araged.clean.Either
import com.araged.failurehandler.ListEmptyThereIsNoInternet
import com.araged.failurehandler.message
import com.araged.movie.data.MovieDataSource
import com.argaed.database.movie.dao.MovieDao
import com.araged.movie.domian.use_case.get_movie_details.GetMovieDetailsFailure
import com.araged.movie.domian.use_case.get_movie_details.GetMovieDetailsResponse
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularFailure
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularResponse
import com.araged.movie.domian.use_case.get_movie_rated.GetMovieRatedFailure
import com.araged.movie.domian.use_case.get_movie_rated.GetMovieRatedResponse
import com.araged.movie.domian.use_case.get_movie_upcoming.GetMovieUpcomingFailure
import com.araged.movie.domian.use_case.get_movie_upcoming.GetMovieUpcomingResponse
import javax.inject.Inject

class MovieDataSourceLocal @Inject constructor(
    private val movieDao: MovieDao
) : MovieDataSource {

    override suspend fun getMoviePopular():
            Either<GetMoviePopularFailure, GetMoviePopularResponse> = try {
        val movieRated = movieDao.getMovieRated()
        if (movieRated.isEmpty()) throw ListEmptyThereIsNoInternet()
        Either.Right(GetMoviePopularResponse(movieRated.map { it.toMovie() }))
    } catch (exception: Exception) {
        val failure: GetMoviePopularFailure = when (exception) {
            is ListEmptyThereIsNoInternet -> GetMoviePopularFailure.NetworkConnectionFailure
            else -> GetMoviePopularFailure.UnknownFailure(exception.message())
        }
        Either.Left(failure)
    }

    override suspend fun getMovieRated():
            Either<GetMovieRatedFailure, GetMovieRatedResponse> = try {
        val movieRated = movieDao.getMovieRated()
        if (movieRated.isEmpty()) throw ListEmptyThereIsNoInternet()
        Either.Right(GetMovieRatedResponse(movieRated.map { it.toMovie() }))
    } catch (exception: Exception) {
        val failure: GetMovieRatedFailure = when (exception) {
            is ListEmptyThereIsNoInternet -> GetMovieRatedFailure.NetworkConnectionFailure
            else -> GetMovieRatedFailure.UnknownFailure(exception.message())
        }
        Either.Left(failure)
    }
    override suspend fun getMovieUpcoming(): Either<GetMovieUpcomingFailure, GetMovieUpcomingResponse> =
        try {
            val movieRated = movieDao.getMovieRated()
            if (movieRated.isEmpty()) throw ListEmptyThereIsNoInternet()
            Either.Right(GetMovieUpcomingResponse(movieRated.map { it.toMovie() }))
        } catch (exception: Exception) {
            val failure: GetMovieUpcomingFailure = when (exception) {
                is ListEmptyThereIsNoInternet -> GetMovieUpcomingFailure.NetworkConnectionFailure
                else -> GetMovieUpcomingFailure.UnknownFailure(exception.message())
            }
            Either.Left(failure)
        }

    override suspend fun getMovieDetails(movieID: Int):
            Either<GetMovieDetailsFailure, GetMovieDetailsResponse> =
        Either.Left(GetMovieDetailsFailure.NetworkConnectionFailure)

}