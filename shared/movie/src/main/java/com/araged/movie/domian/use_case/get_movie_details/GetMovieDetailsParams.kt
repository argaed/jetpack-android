package com.araged.movie.domian.use_case.get_movie_details

data class GetMovieDetailsParams(val movieId: Int)