package com.araged.movie.data.data_source.remote

import android.util.Log
import com.araged.clean.Either
import com.araged.httpclient.retrofit.retrofitApiCall
import com.araged.movie.data.MovieDataSource
import com.argaed.database.movie.dao.MovieDao
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularFailure
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularResponse
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject
import com.araged.failurehandler.message
import com.araged.movie.domian.use_case.get_movie_details.GetMovieDetailsFailure
import com.araged.movie.domian.use_case.get_movie_details.GetMovieDetailsResponse
import com.araged.movie.domian.use_case.get_movie_rated.GetMovieRatedFailure
import com.araged.movie.domian.use_case.get_movie_rated.GetMovieRatedResponse
import com.araged.movie.domian.use_case.get_movie_upcoming.GetMovieUpcomingFailure
import com.araged.movie.domian.use_case.get_movie_upcoming.GetMovieUpcomingResponse
import com.argaed.database.movie.entity.MovieDB
import kotlinx.coroutines.delay
import okhttp3.internal.wait

class MovieDataSourceRemote @Inject constructor(
    private val movieApiServices: MovieApiServices,
    private val movieDao: MovieDao,
) : MovieDataSource {

    override suspend fun getMoviePopular():
            Either<GetMoviePopularFailure, GetMoviePopularResponse> =
        try {
            retrofitApiCall {
                movieApiServices.getMoviePopular()
            }.let {

                it.results.forEach { movie ->
                    val movieDB = MovieDB(
                        overview = movie.overview,
                        originalLanguage = movie.originalLanguage,
                        originalTitle = movie.originalTitle,
                        video = movie.video,
                        title = movie.title,
                        posterPath = movie.posterPath,
                        backdropPath = movie.backdropPath,
                        releaseDate = movie.releaseDate,
                        popularity = movie.popularity,
                        voteAverage = movie.voteAverage,
                        id = movie.id,
                        adult = movie.adult,
                        voteCount = movie.voteCount,
                        category = "Popular"
                    )
                    movieDao.createMovieResult(movieDB)
                }
                Either.Right(GetMoviePopularResponse(it.results.map { movies -> movies.toMovie() }))
            }
        } catch (exception: Exception) {
            val failure: GetMoviePopularFailure = when (exception) {
                is HttpException -> exception.toGetMoviePopularFailure()
                else -> GetMoviePopularFailure.UnknownFailure(exception.message())
            }
            Either.Left(failure)
        }

    override suspend fun getMovieRated(): Either<GetMovieRatedFailure, GetMovieRatedResponse> =
        try {
            retrofitApiCall {
                movieApiServices.getMovieTopRated()
            }.let {
                it.results.forEach { movie ->
                    val movieDB = MovieDB(
                        overview = movie.overview,
                        originalLanguage = movie.originalLanguage,
                        originalTitle = movie.originalTitle,
                        video = movie.video,
                        title = movie.title,
                        posterPath = movie.posterPath,
                        backdropPath = movie.backdropPath,
                        releaseDate = movie.releaseDate,
                        popularity = movie.popularity,
                        voteAverage = movie.voteAverage,
                        id = movie.id,
                        adult = movie.adult,
                        voteCount = movie.voteCount,
                        category = "Rated"

                    )
                    movieDao.createMovieResult(movieDB)
                }
                Either.Right(GetMovieRatedResponse(it.results.map { movies -> movies.toMovie() }))
            }
        } catch (exception: Exception) {
            Timber.e(exception)
            val failure: GetMovieRatedFailure = when (exception) {
                is HttpException -> exception.toGetMovieRatedFailure()
                else -> GetMovieRatedFailure.UnknownFailure(exception.message())
            }
            Either.Left(failure)
        }

    override suspend fun getMovieUpcoming(): Either<GetMovieUpcomingFailure, GetMovieUpcomingResponse> =
        try {
            retrofitApiCall {
                movieApiServices.getMovieUpcoming()
            }.let {
                it.results.forEach { movie ->
                    val movieDB = MovieDB(
                        overview = movie.overview,
                        originalLanguage = movie.originalLanguage,
                        originalTitle = movie.originalTitle,
                        video = movie.video,
                        title = movie.title,
                        posterPath = movie.posterPath,
                        backdropPath = movie.backdropPath,
                        releaseDate = movie.releaseDate,
                        popularity = movie.popularity,
                        voteAverage = movie.voteAverage,
                        id = movie.id,
                        adult = movie.adult,
                        voteCount = movie.voteCount,
                        category = "Upcoming"
                    )
                    movieDao.createMovieResult(movieDB)
                }
                Either.Right(GetMovieUpcomingResponse(it.results.map { movies -> movies.toMovie() }))
            }
        } catch (exception: Exception) {
            Timber.e(exception)
            val failure: GetMovieUpcomingFailure = when (exception) {
                is HttpException -> exception.toGetMovieUpcomingFailure()
                else -> GetMovieUpcomingFailure.UnknownFailure(exception.message())
            }
            Either.Left(failure)
        }

    override suspend fun getMovieDetails(movieID: Int): Either<GetMovieDetailsFailure, GetMovieDetailsResponse> =
        try {
            retrofitApiCall {
                val apiKey = "0a05362c0528a291af53ffd6bb8f64c7"
                movieApiServices.getMovieDetails(movieID, apiKey)
            }.let {
                Either.Right(GetMovieDetailsResponse(it.toMovieDetails()))
            }
        } catch (exception: Exception) {
            Timber.e(exception)
            val failure: GetMovieDetailsFailure = when (exception) {
                is HttpException -> exception.toGetMovieDetailsFailure()
                else -> GetMovieDetailsFailure.UnknownFailure(exception.message())
            }
            Either.Left(failure)
        }
}