package com.araged.show_tv.domian.use_case.get_tv_show_popular

import com.araged.clean.Failure
import com.araged.failurehandler.HttpFailure
import com.araged.failurehandler.NetworkFailure


sealed class GetTvShowPopularFailure : Failure() {

    
    object NetworkConnectionFailure : GetTvShowPopularFailure(), NetworkFailure

    
    object UserDisabled : GetTvShowPopularFailure()

    
    object UserTimeOut : GetTvShowPopularFailure()

    

    /**
     *
     */
    data class ServerFailure(
        override val code: Int,
        override val message: String
    ) : GetTvShowPopularFailure(), HttpFailure

    
    data class UnknownFailure(
        val message: String
    ) : GetTvShowPopularFailure()

}