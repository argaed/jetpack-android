package com.araged.show_tv.data

import com.araged.clean.Either
import com.araged.show_tv.domian.use_case.get_tv_show_details.GetTvShowDetailsFailure
import com.araged.show_tv.domian.use_case.get_tv_show_details.GetTvShowDetailsResponse
import com.araged.show_tv.domian.use_case.get_tv_show_popular.GetTvShowPopularFailure
import com.araged.show_tv.domian.use_case.get_tv_show_popular.GetTvShowPopularResponse
import com.araged.show_tv.domian.use_case.get_tv_show_rated.GetTvShowRatedFailure
import com.araged.show_tv.domian.use_case.get_tv_show_rated.GetTvShowRatedResponse
import com.araged.show_tv.domian.use_case.get_tv_show_latest.GetTvShowLatestFailure
import com.araged.show_tv.domian.use_case.get_tv_show_latest.GetTvShowLatestResponse



interface TvShowDataSource {

    
    suspend fun getTvShowPopular(
    ): Either<GetTvShowPopularFailure, GetTvShowPopularResponse>

    
    suspend fun getTvShowRated(
    ): Either<GetTvShowRatedFailure, GetTvShowRatedResponse>

    
    suspend fun getTvShowLatest(
    ): Either<GetTvShowLatestFailure, GetTvShowLatestResponse>

    
    suspend fun getTvShowDetails(
        movieID: Int
    ): Either<GetTvShowDetailsFailure, GetTvShowDetailsResponse>

}