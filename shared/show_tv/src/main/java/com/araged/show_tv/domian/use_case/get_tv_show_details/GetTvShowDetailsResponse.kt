package com.araged.show_tv.domian.use_case.get_tv_show_details

import com.argaed.entities.tv.ShowTvDetails


data class GetTvShowDetailsResponse(
    val tvShowDetails: ShowTvDetails
)
