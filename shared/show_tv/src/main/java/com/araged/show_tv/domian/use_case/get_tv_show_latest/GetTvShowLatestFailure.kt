package com.araged.show_tv.domian.use_case.get_tv_show_latest

import com.araged.clean.Failure
import com.araged.failurehandler.HttpFailure
import com.araged.failurehandler.NetworkFailure


/* */
sealed class GetTvShowLatestFailure : Failure() {

    
    object NetworkConnectionFailure : GetTvShowLatestFailure(), NetworkFailure

    
    object UserDisabled : GetTvShowLatestFailure()

    
    data class ServerFailure(
        override val code: Int,
        override val message: String
    ) : GetTvShowLatestFailure(), HttpFailure

    
    data class UnknownFailure(
        val message: String
    ) : GetTvShowLatestFailure()

}