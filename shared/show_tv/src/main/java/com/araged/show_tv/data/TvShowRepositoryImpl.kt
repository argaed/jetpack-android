package com.araged.show_tv.data

import com.araged.clean.Either
import com.araged.show_tv.domian.use_case.get_tv_show_details.GetTvShowDetailsFailure
import com.araged.show_tv.domian.use_case.get_tv_show_popular.GetTvShowPopularFailure
import com.araged.show_tv.domian.use_case.get_tv_show_rated.GetTvShowRatedFailure
import com.araged.network.broadcast_receiver.InternetConnectionRepository
import com.araged.show_tv.data.data_source.local.TvShowDataSourceLocal
import com.araged.show_tv.data.data_source.remote.TvShowDataSourceRemote
import com.araged.show_tv.domian.TvShowRepository
import com.araged.show_tv.domian.use_case.get_tv_show_details.GetTvShowDetailsResponse
import com.araged.show_tv.domian.use_case.get_tv_show_latest.GetTvShowLatestFailure
import com.araged.show_tv.domian.use_case.get_tv_show_latest.GetTvShowLatestResponse
import com.araged.show_tv.domian.use_case.get_tv_show_popular.GetTvShowPopularResponse
import com.araged.show_tv.domian.use_case.get_tv_show_rated.GetTvShowRatedResponse
import javax.inject.Inject


class TvShowRepositoryImpl @Inject constructor(
    private val tvShowDataSourceRemote: TvShowDataSourceRemote,
    private val tvShowDataSourceLocal: TvShowDataSourceLocal,
    internetConnectionRepository: InternetConnectionRepository
) : TvShowRepository, InternetConnectionRepository by internetConnectionRepository {

    /**
     * Select the data source depending on the network connection
     * */
    private suspend fun getDataSource(): TvShowDataSource {
        fetch()
        return if (isOnline) tvShowDataSourceRemote else tvShowDataSourceLocal
    }


    override suspend fun getTvShowPopular():
            Either<GetTvShowPopularFailure, GetTvShowPopularResponse> =
        getDataSource().getTvShowPopular()


    override suspend fun getTvShowRated():
            Either<GetTvShowRatedFailure, GetTvShowRatedResponse> =
        getDataSource().getTvShowRated()


    override suspend fun getMovieLatest():
            Either<GetTvShowLatestFailure, GetTvShowLatestResponse> =
        getDataSource().getTvShowLatest()


    override suspend fun getTvShowDetails(movieID: Int):
            Either<GetTvShowDetailsFailure, GetTvShowDetailsResponse> =
        getDataSource().getTvShowDetails(movieID)

}