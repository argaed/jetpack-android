package com.araged.show_tv.domian.use_case.get_tv_show_popular

import com.argaed.entities.tv.ShowTv

data class GetTvShowPopularResponse(
    val tvShows: List<ShowTv>
)
