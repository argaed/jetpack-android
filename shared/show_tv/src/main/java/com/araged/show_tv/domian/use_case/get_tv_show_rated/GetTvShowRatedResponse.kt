package com.araged.show_tv.domian.use_case.get_tv_show_rated

import com.argaed.entities.tv.ShowTv


/* */
data class GetTvShowRatedResponse(
    val tvShows: List<ShowTv>
)
