package com.araged.show_tv.data.data_source.remote

import com.araged.clean.Either
import com.araged.httpclient.retrofit.retrofitApiCall
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject
import com.araged.failurehandler.message
import com.araged.show_tv.domian.use_case.get_tv_show_details.GetTvShowDetailsFailure
import com.araged.show_tv.domian.use_case.get_tv_show_popular.GetTvShowPopularFailure
import com.araged.show_tv.domian.use_case.get_tv_show_rated.GetTvShowRatedFailure
import com.araged.show_tv.data.TvShowDataSource
import com.araged.show_tv.domian.use_case.get_tv_show_details.GetTvShowDetailsResponse
import com.araged.show_tv.domian.use_case.get_tv_show_latest.GetTvShowLatestFailure
import com.araged.show_tv.domian.use_case.get_tv_show_latest.GetTvShowLatestResponse
import com.araged.show_tv.domian.use_case.get_tv_show_popular.GetTvShowPopularResponse
import com.araged.show_tv.domian.use_case.get_tv_show_rated.GetTvShowRatedResponse
import com.argaed.database.show_tv.dao.TvShowDao
import com.argaed.database.show_tv.entity.ShowTvBD

class TvShowDataSourceRemote @Inject constructor(
    private val tvShowApiServices: TvShowApiServices,
    private val tvShowDao: TvShowDao
) : TvShowDataSource {


    override suspend fun getTvShowPopular():
            Either<GetTvShowPopularFailure, GetTvShowPopularResponse> =
        try {
            retrofitApiCall {
                val apiKey = "0a05362c0528a291af53ffd6bb8f64c7"
                tvShowApiServices.getTvShowPopular(apiKey)
            }.let {
                it.results.forEach { response ->
                    val showTvBD = ShowTvBD(
                        backdropPath = response.backdropPath,
                        firstAirDate = response.firstAirDate,
                        id = response.id,
                        name = response.name,
                        originalLanguage = response.originalLanguage,
                        originalName = response.originalName,
                        overview = response.overview,
                        popularity = response.popularity,
                        posterPath = response.posterPath ?:"",
                        voteAverage = response.voteAverage,
                        voteCount = response.voteCount,
                        category = "Popular"
                    )
                    tvShowDao.createTvShowResult(showTvBD)
                }
                Either.Right(GetTvShowPopularResponse(it.results.map { showTv -> showTv.toShowTv() }))
            }
        } catch (exception: Exception) {
            Timber.e(exception)
            val failure: GetTvShowPopularFailure = when (exception) {
                is HttpException -> exception.toGetTvShowPopularFailure()
                else -> GetTvShowPopularFailure.UnknownFailure(exception.message())
            }
            Either.Left(failure)
        }


    override suspend fun getTvShowRated(): Either<GetTvShowRatedFailure, GetTvShowRatedResponse> =
        try {
            retrofitApiCall {
                val apiKey = "0a05362c0528a291af53ffd6bb8f64c7"
                tvShowApiServices.getTvShowTopRated(apiKey)
            }.let {
                it.results.forEach {response ->
                    val showTvBD = ShowTvBD(
                        backdropPath = response.backdropPath,
                        firstAirDate = response.firstAirDate,
                        id = response.id,
                        name = response.name,
                        originalLanguage = response.originalLanguage,
                        originalName = response.originalName,
                        overview = response.overview,
                        popularity = response.popularity,
                        posterPath = response.posterPath ?:"",
                        voteAverage = response.voteAverage,
                        voteCount = response.voteCount,
                        category = "Rated"
                    )
                    tvShowDao.createTvShowResult(showTvBD)
                }
                Either.Right(GetTvShowRatedResponse(it.results.map { showTv -> showTv.toShowTv() }))
            }
        } catch (exception: Exception) {
            Timber.e(exception)
            val failure: GetTvShowRatedFailure = when (exception) {
                is HttpException -> exception.toGetTvShowRatedFailure()
                else -> GetTvShowRatedFailure.UnknownFailure(exception.message())
            }
            Either.Left(failure)
        }


    override suspend fun getTvShowLatest(): Either<GetTvShowLatestFailure, GetTvShowLatestResponse> =
        try {
            retrofitApiCall {
                val apiKey = "0a05362c0528a291af53ffd6bb8f64c7"
                tvShowApiServices.getTvShowUpcoming(apiKey)
            }.let {
                it.results.forEach {response ->
                    val showTvBD = ShowTvBD(
                        backdropPath = response.backdropPath,
                        firstAirDate = response.firstAirDate,
                        id = response.id,
                        name = response.name,
                        originalLanguage = response.originalLanguage,
                        originalName = response.originalName,
                        overview = response.overview,
                        popularity = response.popularity,
                        posterPath = response.posterPath ?:"",
                        voteAverage = response.voteAverage,
                        voteCount = response.voteCount,
                        category = "Upcoming"
                    )
                    tvShowDao.createTvShowResult(showTvBD)
                }
                Either.Right(GetTvShowLatestResponse(it.results.map { showTv -> showTv.toShowTv() }))
            }
        } catch (exception: Exception) {
            Timber.e(exception)
            val failure: GetTvShowLatestFailure = when (exception) {
                is HttpException -> exception.toGetTvShowUpcomingFailure()
                else -> GetTvShowLatestFailure.UnknownFailure(exception.message())
            }
            Either.Left(failure)
        }


    override suspend fun getTvShowDetails(movieID: Int): Either<GetTvShowDetailsFailure, GetTvShowDetailsResponse> =
        try {
            retrofitApiCall {
                val apiKey = "0a05362c0528a291af53ffd6bb8f64c7"
                tvShowApiServices.getTvShowDetails(movieID, apiKey)
            }.let {
                Either.Right(GetTvShowDetailsResponse(it.toTvShowDetails()))
            }
        } catch (exception: Exception) {
            Timber.e(exception)
            val failure: GetTvShowDetailsFailure = when (exception) {
                is HttpException -> exception.toGetTvShowDetailsFailure()
                else -> GetTvShowDetailsFailure.UnknownFailure(exception.message())
            }
            Either.Left(failure)
        }
}