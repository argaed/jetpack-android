package com.araged.show_tv.domian.use_case.get_tv_show_latest

import com.argaed.entities.tv.ShowTv


data class GetTvShowLatestResponse(
    val tvShows: List<ShowTv>
)
