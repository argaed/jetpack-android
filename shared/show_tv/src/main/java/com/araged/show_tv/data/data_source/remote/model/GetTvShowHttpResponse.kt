package com.araged.show_tv.data.data_source.remote.model


import com.araged.show_tv.data.data_source.remote.model.dto.ShowTvDto
import com.argaed.entities.tv.ShowTv
import com.google.gson.annotations.SerializedName


data class GetTvShowHttpResponse(
    @SerializedName( "page") val page: Int,
    @SerializedName( "results") val results: List<ShowTvDto>,
    @SerializedName( "total_pages") val totalPages: Int,
    @SerializedName( "total_results") val totalResults: Int
) {

    fun toTvShow() :List<ShowTv> = results.map { it.toShowTv() }
}