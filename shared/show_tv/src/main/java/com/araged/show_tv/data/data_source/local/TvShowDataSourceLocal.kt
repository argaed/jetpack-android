package com.araged.show_tv.data.data_source.local

import com.araged.clean.Either
import com.araged.failurehandler.ListEmptyThereIsNoInternet
import com.araged.failurehandler.message
import com.araged.show_tv.domian.use_case.get_tv_show_details.GetTvShowDetailsFailure
import com.araged.show_tv.domian.use_case.get_tv_show_details.GetTvShowDetailsResponse
import com.araged.show_tv.domian.use_case.get_tv_show_popular.GetTvShowPopularFailure
import com.araged.show_tv.domian.use_case.get_tv_show_popular.GetTvShowPopularResponse
import com.araged.show_tv.domian.use_case.get_tv_show_rated.GetTvShowRatedFailure
import com.araged.show_tv.domian.use_case.get_tv_show_rated.GetTvShowRatedResponse
import com.araged.show_tv.domian.use_case.get_tv_show_latest.GetTvShowLatestResponse
import com.araged.show_tv.data.TvShowDataSource
import com.araged.show_tv.domian.use_case.get_tv_show_latest.GetTvShowLatestFailure
import com.argaed.database.show_tv.dao.TvShowDao
import javax.inject.Inject

class TvShowDataSourceLocal @Inject constructor(
    private val tvShowDao: TvShowDao
) : TvShowDataSource {

    
    override suspend fun getTvShowPopular():
            Either<GetTvShowPopularFailure, GetTvShowPopularResponse> = try {
        val movieRated = tvShowDao.getTvShowPopular()
        //if (movieRated.isEmpty()) throw ListEmptyThereIsNoInternet()
        Either.Right(GetTvShowPopularResponse(movieRated.map { it.toShowTv() }))
    } catch (exception: Exception) {
        val failure: GetTvShowPopularFailure = when (exception) {
            is ListEmptyThereIsNoInternet -> GetTvShowPopularFailure.NetworkConnectionFailure
            else -> GetTvShowPopularFailure.UnknownFailure(exception.message())
        }
        Either.Left(failure)
    }

    
    override suspend fun getTvShowRated():
            Either<GetTvShowRatedFailure, GetTvShowRatedResponse> = try {
        val movieRated = tvShowDao.getTvShowRated()
        if (movieRated.isEmpty()) throw ListEmptyThereIsNoInternet()
        Either.Right(GetTvShowRatedResponse(movieRated.map { it.toShowTv() }))
    } catch (exception: Exception) {
        val failure: GetTvShowRatedFailure = when (exception) {
            is ListEmptyThereIsNoInternet -> GetTvShowRatedFailure.NetworkConnectionFailure
            else -> GetTvShowRatedFailure.UnknownFailure(exception.message())
        }
        Either.Left(failure)
    }

    
    override suspend fun getTvShowLatest(): Either<GetTvShowLatestFailure, GetTvShowLatestResponse> =
        try {
            val movieRated = tvShowDao.getTvShowLatest()
            if (movieRated.isEmpty()) throw ListEmptyThereIsNoInternet()
            Either.Right(GetTvShowLatestResponse(movieRated.map { it.toShowTv() }))
        } catch (exception: Exception) {
            val failure: GetTvShowLatestFailure = when (exception) {
                is ListEmptyThereIsNoInternet -> GetTvShowLatestFailure.NetworkConnectionFailure
                else -> GetTvShowLatestFailure.UnknownFailure(exception.message())
            }
            Either.Left(failure)
        }

    
    override suspend fun getTvShowDetails(tvShowID: Int):
            Either<GetTvShowDetailsFailure, GetTvShowDetailsResponse> =
        Either.Left(GetTvShowDetailsFailure.NetworkConnectionFailure)

}