package com.araged.show_tv.domian.use_case.get_tv_show_popular

import com.araged.clean.Either
import com.araged.clean.UseCase
import com.araged.movie.domian.use_case.get_movie_popular.GetTvShowPopularParams
import com.araged.show_tv.domian.TvShowRepository
import javax.inject.Inject


class GetTvShowPopularUseCase @Inject constructor(
    private val tvShowRepository: TvShowRepository
) : UseCase<GetTvShowPopularResponse, GetTvShowPopularParams, GetTvShowPopularFailure>() {

    override suspend fun run(
        params: GetTvShowPopularParams
    ): Either<GetTvShowPopularFailure, GetTvShowPopularResponse> =
        tvShowRepository.getTvShowPopular()
}