package com.araged.show_tv.domian.use_case.get_tv_show_rated

import com.araged.clean.Either
import com.araged.clean.UseCase
import com.araged.show_tv.domian.TvShowRepository
import javax.inject.Inject


class GetTvShowRatedUseCase @Inject constructor(
    private val tvShowRepository: TvShowRepository
) : UseCase<GetTvShowRatedResponse, GetTvShowRatedParams, GetTvShowRatedFailure>() {

    override suspend fun run(
        params: GetTvShowRatedParams
    ): Either<GetTvShowRatedFailure, GetTvShowRatedResponse> =
        tvShowRepository.getTvShowRated()
}