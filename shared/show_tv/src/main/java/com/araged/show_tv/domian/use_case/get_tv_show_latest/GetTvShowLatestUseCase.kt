package com.araged.show_tv.domian.use_case.get_tv_show_latest

import com.araged.clean.Either
import com.araged.clean.UseCase
import com.araged.movie.domian.use_case.get_movie_upcoming.GetTvShowLatestParams
import com.araged.show_tv.domian.TvShowRepository
import javax.inject.Inject


class GetTvShowLatestUseCase @Inject constructor(
    private val tvShowRepository: TvShowRepository
) : UseCase<GetTvShowLatestResponse, GetTvShowLatestParams, GetTvShowLatestFailure>() {

    override suspend fun run(
        params: GetTvShowLatestParams
    ): Either<GetTvShowLatestFailure, GetTvShowLatestResponse> =
        tvShowRepository.getMovieLatest()
}