package com.araged.show_tv.domian.use_case.get_tv_show_details

import com.araged.clean.Failure
import com.araged.failurehandler.HttpFailure
import com.araged.failurehandler.NetworkFailure


/* */
sealed class GetTvShowDetailsFailure : Failure() {

    
    object NetworkConnectionFailure : GetTvShowDetailsFailure(), NetworkFailure

    
    object UserDisabled : GetTvShowDetailsFailure()

    
    object UserTimeOut : GetTvShowDetailsFailure()

    /**
     *
     */
    data class ServerFailure(
        override val code: Int,
        override val message: String
    ) : GetTvShowDetailsFailure(), HttpFailure

    
    data class UnknownFailure(
        val message: String
    ) : GetTvShowDetailsFailure()

}