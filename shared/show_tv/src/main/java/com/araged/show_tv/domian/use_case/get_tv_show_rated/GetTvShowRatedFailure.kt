package com.araged.show_tv.domian.use_case.get_tv_show_rated

import com.araged.clean.Failure
import com.araged.failurehandler.HttpFailure
import com.araged.failurehandler.NetworkFailure


sealed class GetTvShowRatedFailure : Failure() {

    
    object NetworkConnectionFailure : GetTvShowRatedFailure(),  NetworkFailure

    
    object UserDisabled : GetTvShowRatedFailure()

    
    object UserTimeOut : GetTvShowRatedFailure()

    

    /**
     *
     */
    data class ServerFailure(
        override val code: Int,
        override val message: String
    ) : GetTvShowRatedFailure(), HttpFailure

    
    data class UnknownFailure(
        val message: String
    ) : GetTvShowRatedFailure()

}