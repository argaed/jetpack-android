package com.araged.show_tv.domian.use_case.get_tv_show_details

import com.araged.clean.Either
import com.araged.clean.UseCase
import com.araged.show_tv.domian.TvShowRepository
import javax.inject.Inject


/* */
class GetTvShowDetailsUseCase @Inject constructor(
    private val tvShowRepository: TvShowRepository
) : UseCase<GetTvShowDetailsResponse, GetTvShowDetailsParams, GetTvShowDetailsFailure>() {

    
    override suspend fun run(
        params: GetTvShowDetailsParams
    ): Either<GetTvShowDetailsFailure, GetTvShowDetailsResponse> =
        tvShowRepository.getTvShowDetails(params.movieID)
}