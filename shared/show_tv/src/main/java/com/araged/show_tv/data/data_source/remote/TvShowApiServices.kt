package com.araged.show_tv.data.data_source.remote

import com.araged.show_tv.data.data_source.remote.model.GetTvShowDetailsHttpResponse
import com.araged.show_tv.data.data_source.remote.model.GetTvShowHttpResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TvShowApiServices {

    
    @GET(URL.GET_MOVIE_POPULAR)
    suspend fun getTvShowPopular(
        @Query("api_key") apiKey: String
    ): Response<GetTvShowHttpResponse>


    
    @GET(URL.GET_MOVIE_TOP_RATED)
    suspend fun getTvShowTopRated(
        @Query("api_key") apiKey: String
    ): Response<GetTvShowHttpResponse>

    
    @GET(URL.GET_MOVIE_LATEST)
    suspend fun getTvShowUpcoming(
        @Query("api_key") apiKey: String
    ): Response<GetTvShowHttpResponse>

    
    @GET(URL.GET_MOVIE_DETAILS)
    suspend fun getTvShowDetails(
        @Path("tv_id") tvId: Int,
        @Query("api_key") apiKey: String
    ): Response<GetTvShowDetailsHttpResponse>

    
    private object URL {
        const val GET_MOVIE_POPULAR: String = "tv/popular/"

        const val GET_MOVIE_TOP_RATED: String = "tv/top_rated/"

        const val GET_MOVIE_LATEST: String = "tv/airing_today/"

        const val GET_MOVIE_DETAILS: String = "tv/{tv_id}"

    }
}