package com.araged.show_tv.data.data_source.remote

import com.araged.httpclient.retrofit.HttpErrorCode
import com.araged.httpclient.retrofit.errorMessage
import com.araged.show_tv.domian.use_case.get_tv_show_details.GetTvShowDetailsFailure
import com.araged.show_tv.domian.use_case.get_tv_show_popular.GetTvShowPopularFailure
import com.araged.show_tv.domian.use_case.get_tv_show_rated.GetTvShowRatedFailure
import com.araged.show_tv.domian.use_case.get_tv_show_latest.GetTvShowLatestFailure
import retrofit2.HttpException


internal fun HttpException.toGetTvShowPopularFailure(): GetTvShowPopularFailure =
    when (HttpErrorCode.fromCode(code())) {
        HttpErrorCode.BAD_REQUEST -> GetTvShowPopularFailure.UserDisabled
        else -> GetTvShowPopularFailure.ServerFailure(code(), errorMessage())
    }


internal fun HttpException.toGetTvShowRatedFailure(): GetTvShowRatedFailure =
    when (HttpErrorCode.fromCode(code())) {
        HttpErrorCode.BAD_REQUEST -> GetTvShowRatedFailure.UserDisabled
        else -> GetTvShowRatedFailure.ServerFailure(code(), errorMessage())
    }


internal fun HttpException.toGetTvShowUpcomingFailure(): GetTvShowLatestFailure =
    when (HttpErrorCode.fromCode(code())) {
        HttpErrorCode.BAD_REQUEST -> GetTvShowLatestFailure.UserDisabled
        else -> GetTvShowLatestFailure.ServerFailure(code(), errorMessage())
    }


internal fun HttpException.toGetTvShowDetailsFailure(): GetTvShowDetailsFailure =
    when (HttpErrorCode.fromCode(code())) {
        HttpErrorCode.BAD_REQUEST -> GetTvShowDetailsFailure.UserDisabled
        else -> GetTvShowDetailsFailure.ServerFailure(code(), errorMessage())
    }

