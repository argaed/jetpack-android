plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("kotlin-parcelize")
    id("kotlin-kapt")
    id("com.google.dagger.hilt.android")
}

android {
    compileSdk = Versions.COMPILE_SDK_VERSION
    buildToolsVersion = Versions.BUILD_TOOLS_VERSION

    defaultConfig {
        minSdk = Versions.MIN_SDK_VERSION
        targetSdk = Versions.TARGET_SDK_VERSION
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        // Release production environment. JKS required.
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }

    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    packagingOptions {
        excludes.add("META-INF/*.kotlin_module")
        excludes.add("/META-INF/{AL2.0,LGPL2.1}")
    }
}

dependencies {
    /** KOTLIN */
    /** ANDROID X */
    implementation(AndroidX.CORE)
    implementation(AndroidX.APP_COMPAT)
    /** RETROFIT */
    implementation(Retrofit.RETROFIT)
    implementation(Retrofit.RETROFIT_CONVERTER)
    implementation(Retrofit.OKHTTP3)
    implementation(Retrofit.LOGGING_INTERCEPTOR)

    implementation(AndroidXLifecycle.LIFECYCLE_LIVEDATA_KTX)
    /** DAGGER*/
    testImplementation(Hilt.HILT_TEST)
    kaptTest(Hilt.HILT_COMPILER)

    implementation(Hilt.DAGGER_ANDROID)
    testAnnotationProcessor(Hilt.HILT_COMPILER)
    kapt(Hilt.HILT_COMPILER)

    androidTestImplementation(Hilt.HILT_TEST)
    kaptAndroidTest(Hilt.HILT_COMPILER)


    androidTestImplementation(Hilt.HILT_TEST)
    kaptAndroidTest(Hilt.HILT_COMPILER)
    /** MODULO*/
    implementation(project(SharedPool.HTTP_CLIENT))
    implementation(project(Core.CLEAN))
    implementation(project(Core.FAILURE_HANDLER))
    implementation(project(SharedPool.NETWORK))
    implementation(project(InterfaceAdapter.DATA_BASE))
    implementation(project(EnterpriseBusinessRules.ENTITIES))
    /** DEBUG */
    implementation(Debug.TIMBER)
    /** COROUTINES */
    implementation(Coroutines.ANDROID)
    implementation(Coroutines.CORE)
    implementation(Coroutines.TEST)
    /** ROOM */
    implementation(Room.RUNTIME)
    implementation(Room.KTX)
    implementation(Room.COROUTINES)
    kapt(Room.KAPT_COMPILER)
    /** TESTING */
    testImplementation(Junit.JUNIT)
    androidTestImplementation(AndroidXTesting.JUNIT)

}