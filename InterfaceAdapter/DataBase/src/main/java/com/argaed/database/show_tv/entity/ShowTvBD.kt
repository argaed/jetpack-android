package com.argaed.database.show_tv.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.argaed.entities.tv.ShowTv

@Entity(tableName = "ShowTvBD")
class ShowTvBD(
    @ColumnInfo(name = "backdropPath") val backdropPath: String?,
    @ColumnInfo(name = "firstAirDate") val firstAirDate: String,
    @PrimaryKey
    @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "originalLanguage") val originalLanguage: String,
    @ColumnInfo(name = "originalName") val originalName: String,
    @ColumnInfo(name = "overview") val overview: String,
    @ColumnInfo(name = "popularity") val popularity: Double,
    @ColumnInfo(name = "posterPath") val posterPath: String,
    @ColumnInfo(name = "voteAverage") val voteAverage: Double,
    @ColumnInfo(name = "voteCount") val voteCount: Int,
    @ColumnInfo(name = "category") var category: String = ""
) {

    fun toShowTv() = ShowTv(
        backdropPath = backdropPath,
        firstAirDate = "firstAirDate",
        id = id,
        name = name,
        originalLanguage = originalLanguage,
        originalName = originalName,
        overview = overview,
        popularity = popularity,
        posterPath = posterPath,
        voteAverage = voteAverage,
        voteCount = voteCount,
        category = category
    )
}