package com.argaed.database.show_tv.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.argaed.database.show_tv.entity.ShowTvBD

@Dao
interface TvShowDao {


    @Query("SELECT * FROM ShowTvBD WHERE category =:category")
    fun getTvShowPopular(category: String = "Popular"): List<ShowTvBD>


    @Query("SELECT * FROM ShowTvBD WHERE category =:category")
    fun getTvShowRated(category: String = "Rated"): List<ShowTvBD>


    @Query("SELECT * FROM ShowTvBD WHERE category =:category")
    fun getTvShowLatest(category: String = "latest"): List<ShowTvBD>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun createTvShowResult(resultTvShow: ShowTvBD)
}