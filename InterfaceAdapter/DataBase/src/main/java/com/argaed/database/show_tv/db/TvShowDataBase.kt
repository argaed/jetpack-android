package com.argaed.database.show_tv.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.argaed.database.show_tv.entity.ShowTvBD
import com.argaed.database.show_tv.dao.TvShowDao

@Database(
    entities = [ShowTvBD::class],
    version = 1,
    exportSchema = false
)
abstract class TvShowDataBase : RoomDatabase() {

    
    abstract fun tvShowDao(): TvShowDao

    
    companion object {

        
        fun getInstance(context: Context): TvShowDataBase {
            synchronized(TvShowDataBase::class.java) {
                return Room.databaseBuilder(
                    context.applicationContext,
                    TvShowDataBase::class.java, DB_NAME
                ).fallbackToDestructiveMigration()
                    .build()
            }

        }

        /* */
        private const val DB_NAME = "show_tv.db"
    }

}