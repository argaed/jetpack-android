package com.argaed.database.movie.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.argaed.database.movie.entity.MovieDB

@Dao
interface MovieDao {

    
    @Query("SELECT * FROM Movies WHERE category =:category")
    fun getMoviePopular(category: String = "Popular"): List<MovieDB>

    
    @Query("SELECT * FROM Movies WHERE category =:category")
    fun getMovieRated(category: String = "Rated"): List<MovieDB>

    
    @Query("SELECT * FROM Movies WHERE category =:category")
    fun getMovieUpcoming(category: String = "Upcoming"): List<MovieDB>

    
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun createMovieResult(movieResults: MovieDB)
}