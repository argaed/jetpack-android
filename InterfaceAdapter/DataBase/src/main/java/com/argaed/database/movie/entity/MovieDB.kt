package com.argaed.database.movie.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.argaed.entities.movies.Movie
import java.io.Serializable

@Entity(tableName = "Movies")
class MovieDB(
    @ColumnInfo(name ="overview") val overview: String,
    @ColumnInfo(name ="original_language") val originalLanguage: String,
    @ColumnInfo(name ="original_title") val originalTitle: String,
    @ColumnInfo(name ="video") val video: Boolean,
    @ColumnInfo(name ="title") val title: String,
    @ColumnInfo(name ="poster_path") val posterPath: String,
    @ColumnInfo(name ="backdrop_path") val backdropPath: String?,
    @ColumnInfo(name ="release_date") val releaseDate: String,
    @ColumnInfo(name ="popularity") val popularity: Double,
    @ColumnInfo(name ="vote_average") val voteAverage: Double,
    @PrimaryKey
    @ColumnInfo(name ="id") val id: Int,
    @ColumnInfo(name ="adult") val adult: Boolean,
    @ColumnInfo(name ="vote_count") val voteCount: Int,
    @ColumnInfo(name = "category") var category: String = "",
) : Serializable {




    fun toMovie() = Movie(
        overview = overview,
        originalLanguage = originalLanguage,
        originalTitle = originalTitle,
        video = video,
        title = title,
        posterPath = posterPath,
        backdropPath = backdropPath,
        releaseDate = releaseDate,
        popularity = popularity,
        voteAverage = voteAverage,
        id = id,
        adult = adult,
        voteCount = voteCount
    )
}