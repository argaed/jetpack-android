package com.argaed.database.movie.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.argaed.database.movie.dao.MovieDao
import com.argaed.database.movie.entity.MovieDB

@Database(
    entities = [MovieDB::class],
    version = 1,
    exportSchema = false
)
abstract class MovieDataBase : RoomDatabase() {

    
    abstract fun movieDao(): MovieDao

    
    companion object {

        
        fun getInstance(context: Context): MovieDataBase {
            synchronized(MovieDataBase::class.java) {
                return Room.databaseBuilder(
                    context.applicationContext,
                    MovieDataBase::class.java, DB_NAME
                ).fallbackToDestructiveMigration()
                    .build()
            }

        }

        private const val DB_NAME = "movie.db"
    }

}