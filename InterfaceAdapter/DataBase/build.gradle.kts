plugins {
    id("com.android.library")
    id("kotlin-parcelize")
    id("kotlin-kapt")
    id("org.jetbrains.kotlin.android")
}

android {
    namespace = "com.argaed.database"
    compileSdk = Versions.COMPILE_SDK_VERSION

    defaultConfig {
        minSdk = 23
        targetSdk = Versions.TARGET_SDK_VERSION

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    /** COROUTINES */
    implementation(Coroutines.ANDROID)
    implementation(Coroutines.CORE)
    implementation(Coroutines.TEST)
    /*MODULE*/
    implementation(project(EnterpriseBusinessRules.ENTITIES))
    /** ROOM */
    implementation(Room.RUNTIME)
    implementation(Room.KTX)
    implementation(Room.COROUTINES)
    kapt(Room.KAPT_COMPILER)
    /** TESTING */
    testImplementation(Junit.JUNIT)
    androidTestImplementation(AndroidXTesting.JUNIT)
}