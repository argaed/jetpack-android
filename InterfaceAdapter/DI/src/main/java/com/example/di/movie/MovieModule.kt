package com.example.di.movie

import android.content.Context
import com.araged.movie.data.MovieRepositoryImpl
import com.araged.movie.data.data_source.local.MovieDataSourceLocal
import com.araged.movie.data.data_source.remote.MovieApiServices
import com.araged.movie.data.data_source.remote.MovieDataSourceRemote
import com.araged.movie.domian.MovieRepository
import com.araged.movie.domian.use_case.get_movie_details.GetMovieDetailsUseCase
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularUseCase
import com.araged.movie.domian.use_case.get_movie_rated.GetMovieRatedUseCase
import com.araged.movie.domian.use_case.get_movie_upcoming.GetMovieUpcomingUseCase
import com.araged.network.broadcast_receiver.InternetConnectionRepository
import com.argaed.database.movie.dao.MovieDao
import com.argaed.database.movie.db.MovieDataBase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class MovieModule {

    /** DATA SOURCE **/
    @Provides
    @Singleton
    fun provideMovieDataBase(
        @ApplicationContext appContext: Context
    ) = MovieDataBase.getInstance(appContext)


    @Provides
    @Singleton
    fun bindMovieDao(
        movieDataBase: MovieDataBase
    ): MovieDao = movieDataBase.movieDao()

    /** API SERVICE **/
    @Provides
    @Singleton
    fun provideMovieApiServices(
        retrofit: Retrofit
    ): MovieApiServices = retrofit.create(MovieApiServices::class.java)


    @Provides
    @Singleton
    fun provideMovieDataSourceLocal(
        movieDao: MovieDao
    ): MovieDataSourceLocal =
        MovieDataSourceLocal(movieDao)

    @Provides
    @Singleton
    fun provideMovieDataSourceRemote(
        movieApiServices: MovieApiServices,
        movieDao: MovieDao
    ): MovieDataSourceRemote {
        return MovieDataSourceRemote(movieApiServices, movieDao)
    }

    @Provides
    @Singleton
    fun provideMovieRepository(
        movieDataSourceLocal: MovieDataSourceLocal,
        movieDataSourceRemote: MovieDataSourceRemote,
        internetConnectionRepository: InternetConnectionRepository,
    ): MovieRepository = MovieRepositoryImpl(
        movieDataSourceLocal = movieDataSourceLocal,
        movieDataSourceRemote = movieDataSourceRemote,
        internetConnectionRepository = internetConnectionRepository
    )

    /** USE CASE*/
    @Provides
    fun provideGetMoviePopularUseCase(
        movieRepository: MovieRepository
    ) = GetMoviePopularUseCase(movieRepository)

    @Provides
    fun provideGetMovieRatedUseCase(
        movieRepository: MovieRepository
    ) = GetMovieRatedUseCase(movieRepository)

    @Provides
    fun provideGetMovieUpcomingUseCase(
        movieRepository: MovieRepository
    ) = GetMovieUpcomingUseCase(movieRepository)

    @Provides
    fun provideGetMovieDetailsUseCase(
        movieRepository: MovieRepository
    ) = GetMovieDetailsUseCase(movieRepository)


}