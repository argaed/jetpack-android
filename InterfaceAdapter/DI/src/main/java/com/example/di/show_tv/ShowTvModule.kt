package com.example.di.show_tv

import android.content.Context
import com.araged.network.broadcast_receiver.InternetConnectionRepository
import com.araged.show_tv.data.TvShowRepositoryImpl
import com.araged.show_tv.data.data_source.local.TvShowDataSourceLocal
import com.araged.show_tv.data.data_source.remote.TvShowApiServices
import com.araged.show_tv.data.data_source.remote.TvShowDataSourceRemote
import com.araged.show_tv.domian.TvShowRepository
import com.araged.show_tv.domian.use_case.get_tv_show_details.GetTvShowDetailsUseCase
import com.araged.show_tv.domian.use_case.get_tv_show_latest.GetTvShowLatestUseCase
import com.araged.show_tv.domian.use_case.get_tv_show_popular.GetTvShowPopularUseCase
import com.araged.show_tv.domian.use_case.get_tv_show_rated.GetTvShowRatedUseCase
import com.argaed.database.show_tv.dao.TvShowDao
import com.argaed.database.show_tv.db.TvShowDataBase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class ShowTvModule {

    /** API SERVICE **/
    @Provides
    @Singleton
    fun provideTvShowApiServices(
        retrofit: Retrofit
    ): TvShowApiServices = retrofit.create(TvShowApiServices::class.java)


    /** DATA SOURCE **/
    @Provides
    @Singleton
    fun provideTvShowDataBase(
        @ApplicationContext appContext: Context
    ) = TvShowDataBase.getInstance(appContext)

    @Provides
    @Singleton
    fun provideTvShowDataSourceLocal(
        tvShowDao: TvShowDao
    ): TvShowDataSourceLocal =
        TvShowDataSourceLocal(tvShowDao)

    @Provides
    @Singleton
    fun provideTvShowDataSourceRemote(
        movieApiServices: TvShowApiServices,
        tvShowDao: TvShowDao
    ): TvShowDataSourceRemote {
        return TvShowDataSourceRemote(movieApiServices, tvShowDao)
    }

    @Provides
    @Singleton
    fun bindTvShowDataBase(
        tvShowDataBase: TvShowDataBase
    ): TvShowDao = tvShowDataBase.tvShowDao()


    @Provides
    @Singleton
    fun provideTvSowRepository(
        tvShowDataSourceLocal: TvShowDataSourceLocal,
        tvShowDataSourceRemote: TvShowDataSourceRemote,
        internetConnectionRepository: InternetConnectionRepository,
    ): TvShowRepository =
        TvShowRepositoryImpl(
            tvShowDataSourceLocal = tvShowDataSourceLocal,
            tvShowDataSourceRemote = tvShowDataSourceRemote,
            internetConnectionRepository = internetConnectionRepository
        )


    /** USE CASE*/
    @Provides
    fun provideGetTvShowPopularUseCase(
        tvShowRepository: TvShowRepository
    ) = GetTvShowPopularUseCase(tvShowRepository)

    @Provides
    fun provideGetTvShowRatedUseCase(
        tvShowRepository: TvShowRepository
    ) = GetTvShowRatedUseCase(tvShowRepository)

    @Provides
    fun provideGetTvShowLatestUseCase(
        tvShowRepository: TvShowRepository
    ) = GetTvShowLatestUseCase(tvShowRepository)

    @Provides
    fun provideGetTvShowDetailsUseCase(
        tvShowRepository: TvShowRepository
    ) = GetTvShowDetailsUseCase(tvShowRepository)
}