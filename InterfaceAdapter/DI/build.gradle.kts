plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("kotlin-parcelize")
    id("kotlin-kapt")
    id("com.google.dagger.hilt.android")
}

android {
    namespace = "com.example.di"
    compileSdk = Versions.COMPILE_SDK_VERSION

    defaultConfig {
        minSdk = Versions.MIN_SDK_VERSION
        targetSdk = Versions.TARGET_SDK_VERSION

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    /** ANDROID X */
    implementation(AndroidX.CORE)
    implementation(AndroidX.APP_COMPAT)
    /** RETROFIT */
    implementation(Retrofit.RETROFIT)
    implementation(Retrofit.RETROFIT_CONVERTER)
    implementation(Retrofit.OKHTTP3)
    implementation(Retrofit.LOGGING_INTERCEPTOR)

    implementation(AndroidXLifecycle.LIFECYCLE_LIVEDATA_KTX)
    /** DAGGER*/
    testImplementation(Hilt.HILT_TEST)
    kaptTest(Hilt.HILT_COMPILER)

    implementation(Hilt.DAGGER_ANDROID)
    testAnnotationProcessor(Hilt.HILT_COMPILER)
    kapt(Hilt.HILT_COMPILER)

    androidTestImplementation(Hilt.HILT_TEST)
    kaptAndroidTest(Hilt.HILT_COMPILER)


    androidTestImplementation(Hilt.HILT_TEST)
    kaptAndroidTest(Hilt.HILT_COMPILER)
    /** MODULO*/
    implementation(project(SharedPool.HTTP_CLIENT))
    implementation(project(Core.CLEAN))
    implementation(project(Core.FAILURE_HANDLER))
    implementation(project(SharedPool.NETWORK))
    implementation(project(EnterpriseBusinessRules.ENTITIES))
    implementation(project(InterfaceAdapter.DATA_BASE))
    implementation(project(Shared.MOVIE))
    implementation(project(Shared.TV_SHOW))
    /** DEBUG */
    implementation(Debug.TIMBER)
    /** COROUTINES */
    implementation(Coroutines.ANDROID)
    implementation(Coroutines.CORE)
    implementation(Coroutines.TEST)
    /** ROOM */
    implementation(Room.RUNTIME)
    implementation(Room.KTX)
    implementation(Room.COROUTINES)
    kapt(Room.KAPT_COMPILER)
    /** TESTING */
    testImplementation(Junit.JUNIT)
    androidTestImplementation(AndroidXTesting.JUNIT)

}