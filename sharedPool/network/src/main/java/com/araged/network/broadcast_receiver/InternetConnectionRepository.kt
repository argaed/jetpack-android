package com.araged.network.broadcast_receiver



interface InternetConnectionRepository {

    /* */
    var isOnline: Boolean

    
    suspend fun fetch()

}
