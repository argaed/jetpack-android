package com.araged.network.presentation

import android.app.Application
import android.content.IntentFilter
import com.araged.network.system.broadcast_receiver.ConnectivityReceiver

/**
 *
 */
fun Application.initNetworkConnectionBroadcastReceiver(){
    registerReceiver(
        ConnectivityReceiver(),
        IntentFilter(ConnectivityReceiver.connectivityChangeInputFilter)
    )
}