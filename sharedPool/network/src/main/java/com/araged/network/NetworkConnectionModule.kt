package com.araged.network

import com.araged.network.broadcast_receiver.InternetConnectionRepository
import com.araged.network.broadcast_receiver.InternetConnectionRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
abstract class NetworkConnectionModule {

    @Binds
    abstract fun bindInternetConnectionRepository(
        internetConnectionRepositoryImpl: InternetConnectionRepositoryImpl
    ): InternetConnectionRepository
}