package com.araged.network.broadcast_receiver

import android.content.Context
import com.araged.network.system.network.haveNetworkConnection
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class InternetConnectionRepositoryImpl
@Inject constructor(
    @ApplicationContext private val context: Context
) : InternetConnectionRepository {

    override var isOnline: Boolean = context.haveNetworkConnection()

    
    override suspend fun fetch() {
        isOnline = try {
            context.haveNetworkConnection()
        } catch (exception: Exception) {
            false
        }
    }

}
