package com.araged.network.system.broadcast_receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.araged.network.broadcast_receiver.InternetConnectionRepository
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

/**
 *
 */
@AndroidEntryPoint
internal class ConnectivityReceiver : BroadcastReceiver() {

    @Inject lateinit var internetConnectionRepository: InternetConnectionRepository

    
    override fun onReceive(context: Context?, intent: Intent?) {
        runBlocking { internetConnectionRepository.fetch() }
    }

    
    companion object {
        /* */
        const val connectivityChangeInputFilter: String = "android.net.conn.CONNECTIVITY_CHANGE"
    }

}