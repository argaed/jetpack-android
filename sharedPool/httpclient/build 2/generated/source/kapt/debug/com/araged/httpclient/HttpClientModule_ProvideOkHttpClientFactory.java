// Generated by Dagger (https://dagger.dev).
package com.araged.httpclient;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import okhttp3.OkHttpClient;

@ScopeMetadata("javax.inject.Singleton")
@QualifierMetadata
@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class HttpClientModule_ProvideOkHttpClientFactory implements Factory<OkHttpClient> {
  private final HttpClientModule module;

  public HttpClientModule_ProvideOkHttpClientFactory(HttpClientModule module) {
    this.module = module;
  }

  @Override
  public OkHttpClient get() {
    return provideOkHttpClient(module);
  }

  public static HttpClientModule_ProvideOkHttpClientFactory create(HttpClientModule module) {
    return new HttpClientModule_ProvideOkHttpClientFactory(module);
  }

  public static OkHttpClient provideOkHttpClient(HttpClientModule instance) {
    return Preconditions.checkNotNullFromProvides(instance.provideOkHttpClient());
  }
}
