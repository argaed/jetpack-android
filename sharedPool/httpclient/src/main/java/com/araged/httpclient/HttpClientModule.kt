package com.araged.httpclient

import com.araged.httpclient.retrofit.Authorization
import com.araged.httpclient.retrofit.UrlProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class HttpClientModule {

    private val timeOut: Long = 10000L

    @Provides
    fun provideBaseUrl(urlProvider: UrlProvider): String = urlProvider.baseUrl

    @Provides
    @Singleton
    fun provideOkHttpClient(
        authorization: Authorization
    ): OkHttpClient = if (BuildConfig.DEBUG) {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(getHeaderInterceptor(authorization.authorization))
            .connectTimeout(timeOut, TimeUnit.SECONDS)
            .readTimeout(timeOut, TimeUnit.SECONDS)
            .build()
    } else {
        OkHttpClient
            .Builder()
            .connectTimeout(timeOut, TimeUnit.SECONDS)
            .addInterceptor(getHeaderInterceptor(authorization.authorization))
            .readTimeout(timeOut, TimeUnit.SECONDS)
            .build()
    }

    private fun getHeaderInterceptor(authorization: String) = Interceptor {
        val newRequest = it.request().newBuilder()
            .addHeader("Authorization", "Bearer $authorization")
            .build()
        it.proceed(newRequest)
    }

    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        BASE_URL: String
    ): Retrofit =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .build()

}