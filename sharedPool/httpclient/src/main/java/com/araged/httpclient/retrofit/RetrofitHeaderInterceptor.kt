package com.araged.httpclient.retrofit

/* */
interface RetrofitHeaderInterceptor {

    /**
     *
     */
    fun getAuthorizationType() : String

    /**
     *
     */
    fun getAuthorizationValue() : String?

}
