package com.araged.httpclient.retrofit

data class UrlProvider(
    val baseUrl: String
)

data class Authorization(
    val authorization: String
)

