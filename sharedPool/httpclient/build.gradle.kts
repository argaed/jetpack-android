
plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("kotlin-parcelize")
    id("kotlin-kapt")
    id("com.google.dagger.hilt.android")
}

android {
    compileSdk = Versions.COMPILE_SDK_VERSION
    buildToolsVersion = Versions.BUILD_TOOLS_VERSION

    defaultConfig {
        minSdk = Versions.MIN_SDK_VERSION
        targetSdk = Versions.TARGET_SDK_VERSION
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        // Release production environment. JKS required.
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }

    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }


    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    packagingOptions {
        excludes.add("META-INF/*.kotlin_module")
        excludes.add("/META-INF/{AL2.0,LGPL2.1}")
    }
}

dependencies {
    /** KOTLIN */
    /** ANDROID X */
    implementation(AndroidX.CORE)
    implementation(AndroidX.APP_COMPAT)
    /** RETROFIT */
    implementation(Retrofit.RETROFIT)
    implementation(Retrofit.RETROFIT_CONVERTER)
    implementation(Retrofit.LOGGING_INTERCEPTOR)
    implementation(Retrofit.OKHTTP3)

    /**Dagger*/
    androidTestImplementation(Hilt.HILT_TEST)
    implementation(Hilt.DAGGER_ANDROID)
    implementation(Hilt.HILT_NAVIGATE)
    kapt(Hilt.HILT_COMPILER)
    androidTestImplementation(Hilt.HILT_TEST)
    kaptAndroidTest(Hilt.HILT_COMPILER)
    /** MODULE*/
    implementation(project(Core.FAILURE_HANDLER))
    /** COROUTINES */
    implementation(Coroutines.ANDROID)
    implementation(Coroutines.CORE)
    implementation(Coroutines.TEST)
    /** TESTING */
    testImplementation(Junit.JUNIT)
    androidTestImplementation(AndroidXTesting.JUNIT)

}