package com.araged.resources.presentation.message.snackbar

import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.araged.resources.R
import com.google.android.material.snackbar.Snackbar


fun Fragment.showSnackbar(@StringRes messageRes: Int, duration: Int = Snackbar.LENGTH_LONG) {
    val stringMessage: String = getString(messageRes)
    showSnackbar(stringMessage)
}


fun Fragment.showSnackbar(message: String, duration: Int = Snackbar.LENGTH_LONG) {
    val snackbar = Snackbar.make(view ?: return, message, duration)
    val backgroundColor = ContextCompat.getColor(requireContext(), R.color.purple_700)
    snackbar.view.setBackgroundColor(backgroundColor)
    val textColor = ContextCompat.getColor(requireContext(), android.R.color.white)
    snackbar.setTextColor(textColor)
    snackbar.show()
}