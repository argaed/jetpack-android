plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
}

android {
    compileSdk = Versions.COMPILE_SDK_VERSION
    buildToolsVersion = Versions.BUILD_TOOLS_VERSION

    defaultConfig {
        minSdk = Versions.MIN_SDK_VERSION
        targetSdk = Versions.TARGET_SDK_VERSION
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                    getDefaultProguardFile("proguard-android-optimize.txt"),
                    "proguard-rules.pro"
            )
        }

    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }


}

dependencies {
    /** KOTLIN */
    /** ANDROID X */
    implementation(AndroidX.CORE)
    implementation(AndroidX.APP_COMPAT)
    implementation(Google.MATERIAL)
    /** COMPOSE */
    implementation(AndroidXLifecycle.LIFECYCLE_VIEWMODEL_KTX)
    implementation(AndroidCompose.ANDROID_COMPOSE_UI)
    implementation(AndroidCompose.ANDROID_COMPOSE_COMPILER)
    implementation(AndroidCompose.ANDROID_COMPOSE_MATERIAL)
    implementation(AndroidCompose.ANDROID_COMPOSE_TOOLING)
    implementation(AndroidCompose.ANDROID_COMPOSE_RUNTIME)
    implementation(AndroidCompose.ANDROID_COMPOSE_LIVEDATA)
    implementation(AndroidCompose.ANDROID_COMPOSE_UI_UNIT)
    implementation(AndroidCompose.ANDROID_COMPOSE_ANIMATION)
    implementation(AndroidCompose.CONSTRAINT_COMPOSE)
    implementation(AndroidCompose.ANDROID_COMPOSE_NAVIGATE)
    implementation(AndroidX.WINDOW)
    implementation(AndroidXNavigation.FRAGMENT_KTX)
    implementation(AndroidXNavigation.UI_KTX)
    /** MODULE*/
    implementation(project(Core.FAILURE_HANDLER))
    implementation(project(Core.CLEAN))

}