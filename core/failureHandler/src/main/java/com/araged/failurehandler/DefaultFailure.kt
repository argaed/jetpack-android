package com.araged.failurehandler

/** This class represents any failure that is `Unknown` and is not handled. */
interface DefaultFailure {

	/* */
	val message: String

}