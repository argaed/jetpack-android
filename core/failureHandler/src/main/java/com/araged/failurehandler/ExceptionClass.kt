package com.araged.failurehandler


/**
 * the exception is for when list is empty show the message that there is no internet
 * */
class ListEmptyThereIsNoInternet : Exception()