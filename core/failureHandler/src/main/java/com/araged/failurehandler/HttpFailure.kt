package com.araged.failurehandler


interface HttpFailure {

    /* */
    val code: Int
    /* */
    val message: String

}