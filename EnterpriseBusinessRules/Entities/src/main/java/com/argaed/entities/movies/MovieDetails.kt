package com.argaed.entities.movies

data class MovieDetails(
    val adult: Boolean,
    val backdropPath: String?,
    val budget: Int,
    val homepage: String,
    val id: Int,
    val imdbId: String,
    val originalLanguage: String,
    val originalTitle: String,
    val overview: String,
    val popularity: Double,
    val posterPath: String,
    val releaseDate: String?,
    val revenue: Int,
    val runtime: Int,
    val status: String,
    val tagline: String,
    val title: String,
    val video: Boolean,
    val voteAverage: Double,
    val voteCount: Int
) {

    constructor() : this(
        adult = false,
        backdropPath = "",
        budget = 0,
        homepage = "",
        id = 0,
        imdbId = "",
        originalLanguage = "",
        originalTitle = "",
        overview = "",
        popularity = 0.0,
        posterPath = "",
        releaseDate = null?: "",
        revenue = 0,
        runtime = 0,
        status = "",
        tagline = "",
        title = "",
        video = false,
        voteAverage = 0.0,
        voteCount = 0
    )
}