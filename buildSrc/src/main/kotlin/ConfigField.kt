
/* */
object ConfigField {

	/* */
	const val API_BASE_URL: String = "API_BASE_URL"

	/* */
	const val APPLICATION_VERSION_NAME: String = "APPLICATION_VERSION_NAME"

	/* */
	const val APPLICATION_NUMBER_VERSION: String = "\"${Releases.VERSION_NAME}\""

	/* */
	const val API_KEY: String = "API_KEY"

	/* */
	const val DEVELOP: String = "develop"


	/* */
	val apiBaseUrlMap: Map<String, String> = mapOf(
		//TODO: Use the correct url for each environment when backend provide it
		DEVELOP    to "\"https://api-gw-test.vivaaerobus.io/\""
	).withDefault {
		"\"https://api-gw.vivaaerobus.com/v1/\""
	}


}