/* */
object Debug {

	/* */
	private object Version {

		/* */
		const val TIMBER = "4.7.1"

	}

	/* */
	const val TIMBER = "com.jakewharton.timber:timber:${Version.TIMBER}"

}