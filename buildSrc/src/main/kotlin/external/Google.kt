/* */
object Google {

    /* */
    private object Version {

        /* */
        const val MATERIAL = "1.4.0"

        /* */
        const val SERVICES = "4.3.5"

        /* */
        const val AUTH = "19.0.0"

        /* */
        const val LOCATION = "18.0.0"

        /* */
        const val AR_CORE = "1.23.0"
    }

    /* @see [https://material.io/] */
    const val MATERIAL = "com.google.android.material:material:${Version.MATERIAL}"

    /* */
    const val SERVICES = "com.google.gms:google-services:${Version.SERVICES}"

    /* */
    const val AUTH = "com.google.android.gms:play-services-auth:${Version.AUTH}"

    /* */
    const val LOCATION = "com.google.android.gms:play-services-location:${Version.LOCATION}"

    /* */
    const val AR_CORE = "com.google.ar:core:${Version.AR_CORE}"
}