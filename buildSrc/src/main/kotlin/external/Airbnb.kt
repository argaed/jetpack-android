
/* */
object Airbnb {

	/* */
	private object Version {

		/* */
		const val PARIS = "1.7.3"

		/* */
		const val LOTTIE: String = "3.5.0"

	}

	/* @see [https://github.com/airbnb/paris] */
	const val PARIS = "com.airbnb.android:paris:${Version.PARIS}"

	/* */
	const val LOTTIE = "com.airbnb.android:lottie:${Version.LOTTIE}"

}