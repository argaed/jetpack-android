/* */
object Glide {

    /* */
    private object Version {

        /* */
        const val GLIDE = "4.15.0"

        /* */
        const val GLIDE_LIFECYCLE_COMPILER: String = "1.0.0"

        /* */
        const val GLIDE_COMPILER: String = "4.15.0"

        /* */
        const val LAND_SCAPIST: String = "1.4.3"

    }

    const val GLIDE = "com.github.bumptech.glide:glide:${Version.GLIDE}"

    /* */
    const val GLIDE_LIFECYCLE_COMPILER =
        "com.airbnb.android:lottie:${Version.GLIDE_LIFECYCLE_COMPILER}"

    /* */
    const val GLIDE_COMPILER = "com.github.bumptech.glide:compiler:${Version.GLIDE_COMPILER}"

    /* */
    const val LAND_SCAPIST = "com.github.skydoves:landscapist-glide:${Version.LAND_SCAPIST}"


}