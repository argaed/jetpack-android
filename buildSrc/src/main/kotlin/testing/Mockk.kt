object Mockk {

	
	private object Version {
		const val MOCKK = "1.12.2"
	}

	const val MOCKK = "io.mockk:mockk:${Version.MOCKK}"
}