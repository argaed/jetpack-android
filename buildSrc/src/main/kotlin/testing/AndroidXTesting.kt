/* */
object AndroidXTesting {

	/* */
	private object Version {

		const val JUNIT = "1.1.5"

		const val ESPRESSO_CORE = "3.5.1"

		const val ANDROID_CORE_TESTING = "2.2.0"

	}

	const val JUNIT = "androidx.test.ext:junit-ktx:${Version.JUNIT}"

	const val ANDROID_CORE_TESTING = "androidx.arch.core:core-testing:${Version.ANDROID_CORE_TESTING}"

	/* @see [https://developer.android.com/training/testing/espresso] */
	const val ESPRESSO_CORE = "androidx.test.espresso:espresso-core:${Version.ESPRESSO_CORE}"

}