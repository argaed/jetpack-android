object Hilt {

    /* */
    private object Version {

        /* */
        const val HILT = "2.44"

        const val NAVIGATE = "1.0.0"

    }

    const val DAGGER_GRADLE = "com.google.dagger:hilt-android-gradle-plugin:${Version.HILT}"

    const val HILT_NAVIGATE = "androidx.hilt:hilt-navigation-compose:${Version.NAVIGATE}"

    const val DAGGER_ANDROID = "com.google.dagger:hilt-android:${Version.HILT}"

    const val HILT_TEST = "com.google.dagger:hilt-android-testing:${Version.HILT}"

    const val HILT_COMPILER = "com.google.dagger:hilt-compiler:${Version.HILT}"

}
