
object Dagger {

    /* */
    private object Version {

        /* */
        const val DAGGER2 = "2.24"

    }

    /* */
    const val DAGGER2 = "com.google.dagger:hilt-android-gradle-plugin:${Version.DAGGER2}"

    /* */
    const val DAGGER2_COMPILER = "com.google.dagger:dagger-compiler:${Version.DAGGER2}"

}