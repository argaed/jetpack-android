
/* */
object AndroidTools {

	/* */
	private object Version {

		/* */
		const val BUILD_GRADLE = "7.4.1"

	}

	/* */
	const val BUILD_GRADLE = "com.android.tools.build:gradle:${Version.BUILD_GRADLE}"

}