object AndroidCompose
{
    /* */
    object Version {

        const val ANDROID_COMPOSE = "1.3.3"

        const val ANDROID_COMPOSE_MATERIAL = "1.3.1"

        const val ANDROID_COMPOSE_RUNTIME = "1.3.3"

        const val ANDROID_COMPOSE_NAVIGATE = "2.6.0-alpha05"

        const val ACTIVITY_COMPOSE = "1.3.3"

        const val ANDROID_COMPOSE_COMPILER = "1.4.2"

        const val UI_UNIT = "1.1.0-beta03"

        const val CONSTRAINT_COMPOSE = "1.0.0"

        const val VIEW_MODEL_COMPOSE = "2.6.0"

        const val COMPOSE_UI_MANIFEST = "2.4.0"

        const val COMPOSE_LIFECYCLE_RUNTIME_COMPOSE = "2.6.0"

    }

    const val ANDROID_COMPOSE_UI = "androidx.compose.ui:ui:${Version.ANDROID_COMPOSE}"

    const val ANDROID_COMPOSE_COMPILER = "androidx.compose.compiler:compiler:${Version.ANDROID_COMPOSE_COMPILER}"

    const val ANDROID_COMPOSE_RUNTIME =
        "androidx.compose.runtime:runtime:${Version.ANDROID_COMPOSE_RUNTIME}"

    const val ANDROID_COMPOSE_LIVEDATA =
        "androidx.compose.runtime:runtime-livedata:${Version.ANDROID_COMPOSE_RUNTIME}"

    const val ANDROID_COMPOSE_NAVIGATE = "androidx.navigation:navigation-compose:${Version.ANDROID_COMPOSE_NAVIGATE}"

    const val ANDROID_COMPOSE_MATERIAL =
        "androidx.compose.material:material:${Version.ANDROID_COMPOSE_MATERIAL}"

    const val ANDROID_COMPOSE_TOOLING =
        "androidx.compose.ui:ui-tooling-preview:${Version.ANDROID_COMPOSE}"

    const val ANDROID_COMPOSE_TEST = "androidx.compose.ui:ui-test-junit4:${Version.ANDROID_COMPOSE}"

    const val ANDROID_COMPOSE_DEBUG = "androidx.compose.ui:ui-tooling:${Version.ANDROID_COMPOSE}"

    const val ACTIVITY_COMPOSE = "androidx.activity:activity-compose:${Version.ACTIVITY_COMPOSE}"

    const val ANDROID_COMPOSE_UI_UNIT = "androidx.compose.ui:ui-unit:${Version.UI_UNIT}"

    const val ANDROID_COMPOSE_ANIMATION =
        "androidx.compose.animation:animation:${Version.ANDROID_COMPOSE}"

    const val CONSTRAINT_COMPOSE =
        "androidx.constraintlayout:constraintlayout-compose:${Version.CONSTRAINT_COMPOSE}"

    const val VIEW_MODEL_COMPOSE =
        "androidx.lifecycle:lifecycle-viewmodel-compose:${Version.VIEW_MODEL_COMPOSE}"

    const val COMPOSE_UI_MANIFEST = "androidx.compose.ui:ui-test-manifest:${Version.ANDROID_COMPOSE}"

    const val COMPOSE_UI_JUNIT = "androidx.compose.ui:ui-test-junit4:${Version.ANDROID_COMPOSE}"

    const val COMPOSE_UI_TOOLING = "androidx.compose.ui:ui-tooling:${Version.ANDROID_COMPOSE}"

    const val COMPOSE_LIFECYCLE_RUNTIME_COMPOSE = "androidx.lifecycle:lifecycle-runtime-compose:${Version.COMPOSE_LIFECYCLE_RUNTIME_COMPOSE}"

}