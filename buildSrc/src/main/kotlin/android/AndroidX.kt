/* */
object AndroidX {

    /* */
    private object Version {

        /* */
        const val CORE = "1.9.0"

        /* */
        const val APP_COMPAT = "1.4.0"

        /* */
        const val WINDOW = "1.0.0-beta04"

    }

    /* */
    const val CORE = "androidx.core:core-ktx:${Version.CORE}"

    /* */
    const val APP_COMPAT = "androidx.appcompat:appcompat:${Version.APP_COMPAT}"

    /* */
    const val ACTIVITY = "androidx.activity:activity-ktx:${Version.APP_COMPAT}"

    /* */
    const val WINDOW = "androidx.window:window:${Version.WINDOW}"


}