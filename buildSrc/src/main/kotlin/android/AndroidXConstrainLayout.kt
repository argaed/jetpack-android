
object AndroidXConstrainLayout {

    /* */
    private object Version {

        /* */
        const val CONSTRAINT_LAYOUT = "2.1.2"

    }

    /* */
    const val CONSTRAINT_LAYOUT =
        "androidx.constraintlayout:constraintlayout:${Version.CONSTRAINT_LAYOUT}"
}