/* */
object Coroutines {

	/* */
	private object Version {

		/* */
		const val COROUTINES = "1.6.4"

	}

	/* */
	const val CORE = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Version.COROUTINES}"

	/* */
	const val ANDROID = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Version.COROUTINES}"

	/* */
	const val PLAY_SERVICE =
		"org.jetbrains.kotlinx:kotlinx-coroutines-play-services:${Version.COROUTINES}"

	/* */
	const val TEST = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Version.COROUTINES}"

}