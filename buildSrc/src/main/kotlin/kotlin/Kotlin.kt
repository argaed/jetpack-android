
/* */
object Kotlin {

	/* */
	private object Version {

		/* */
		const val KOTLIN = "1.7.0"

	}

	/* @see [https://kotlinlang.org/docs/reference/android-overview.html] */
	const val STANDARD_LIBRARY = "org.jetbrains.kotlin:kotlin-stdlib:${Version.KOTLIN}"

	/* */
	const val GRADLE_PLUGIN = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Version.KOTLIN}"

	/* */
	const val SERIALIZATION = "org.jetbrains.kotlin:kotlin-serialization:${Version.KOTLIN}"

}