/* */
object FeaturePool {

	/* */
	private const val PREFIX = ":featurePool"

	/* */
	const val ANDROID_EXTENSIONS = "${PREFIX}:android_extensions"

	/* */
	const val INPUT_VALIDATOR = "${PREFIX}:input_validator"

	/* */
	const val NAVIGATION = "${PREFIX}:navigation"

	/* */
	const val NOTIFICATIONS ="${PREFIX}:notifications"

	/* */
	const val RESOURCES = "${PREFIX}:resources"


	/* */
	const val PERMISSIONS = "${PREFIX}:permissions"

}