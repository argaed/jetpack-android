
/* */
object SharedPool {

	/* */
	private const val PREFIX = ":sharedPool"

	/* */
	const val DATABASE = "${PREFIX}:database"

	/* */
	const val ENUMERATIONS = "${PREFIX}:enumerations"

	/* */
	const val HTTP_CLIENT = "${PREFIX}:httpclient"

	/* */
	const val NETWORK = "${PREFIX}:network"

	/* */
	const val SHARED_PREFERENCES = "${PREFIX}:sharedPreferences"
}