
object InterfaceAdapter {

    private const val PREFIX = ":InterfaceAdapter"

    const val DATA_BASE = "${InterfaceAdapter.PREFIX}:DataBase"

    const val DI = "${InterfaceAdapter.PREFIX}:DI"

}