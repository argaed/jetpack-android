/* */
object Core {

	/* */
	private const val PREFIX = ":core"

	/* */
	const val EXTENSION = "${PREFIX}:extension"

	/* */
	const val FAILURE_HANDLER = "${PREFIX}:failureHandler"

	/* */
	const val BASE = "${PREFIX}:base"

	/* */
	const val CLEAN = "${PREFIX}:clean"

}