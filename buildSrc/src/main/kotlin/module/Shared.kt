/* */
object Shared {

    /* */
    private const val PREFIX = ":shared"

    /* */
    const val MOVIE = "${PREFIX}:movie"

    /* */
    const val TV_SHOW = "${PREFIX}:show_tv"

}