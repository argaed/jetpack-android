
/* */
object Retrofit {

	/* */
	private object Version {

		/* */
		const val RETROFIT = "2.9.0"

		/* */
		const val LOGGING_INTERCEPTOR = "4.10.0"

		/* */
		const val MOCK_WEB_SERVER = "4.9.0"

		const val OKHTTP3 = "4.9.1"
	}

	/* */
	const val RETROFIT = "com.squareup.retrofit2:retrofit:${Version.RETROFIT}"

	const val RETROFIT_CONVERTER = "com.squareup.retrofit2:converter-gson:${Version.RETROFIT}"

	const val OKHTTP3 = "com.squareup.okhttp3:okhttp:${Version.OKHTTP3}"

	/* */
	const val LOGGING_INTERCEPTOR =
		"com.squareup.okhttp3:logging-interceptor:${Version.LOGGING_INTERCEPTOR}"

	/* */
	const val MOCK_WEB_SERVER = "com.squareup.okhttp3:mockwebserver:${Version.MOCK_WEB_SERVER}"

}