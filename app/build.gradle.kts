plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("kotlin-parcelize")
    id("kotlin-kapt")
    id("com.google.dagger.hilt.android")
}

android {
    compileSdk = Versions.COMPILE_SDK_VERSION
    buildToolsVersion = Versions.BUILD_TOOLS_VERSION

    defaultConfig {
        applicationId = "com.araged.jetpack"
        minSdk = Versions.MIN_SDK_VERSION
        targetSdk = Versions.TARGET_SDK_VERSION
        versionCode = Releases.VERSION_CODE
        versionName = Releases.VERSION_NAME
        testInstrumentationRunner = "com.araged.jetpack.utils.CustomTestRunner"
    }

    buildTypes {
        // Release production environment. JKS required.
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }

        // Debug environment
        getByName("debug") {
            isDebuggable = true
        }
    }

    flavorDimensions("version")
    productFlavors {
        create("develop") {
            dimension = "version"
            versionNameSuffix = "-dev"
            resValue("string", "app_name_res", "JetPack [DEV]")
            buildConfigField(
                "String",
                "API_BASE_URL",
                "\"https://api.themoviedb.org/3/\""
            )
            buildConfigField(
                "String",
                "Authorization",
                "\"eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIwYTA1MzYyYzA1MjhhMjkxYWY1M2ZmZDZiYjhmNjRjNyIsIm5iZiI6MTcyNjE3MjA4OS4xNDM0MjUsInN1YiI6IjYxODE2NzM3M2ZhYmEwMDAyYzNlMjJjMSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.0WSP7Hgfme1a_u3DHAclmomdkkEhq-l9ucCd3HgFslI\""
            )
        }

    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.3"
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    buildFeatures {
        compose = true
    }

    hilt {
        enableTransformForLocalTests = true
    }

    packagingOptions {
        excludes.add("META-INF/*.kotlin_module")
        excludes.add("/META-INF/{AL2.0,LGPL2.1}")
        excludes.add("META-INF/proguard/androidx-annotations.pro")
        excludes.add("META-INF/DEPENDENCIES")
        excludes.add("META-INF/LICENSE")
        excludes.add("META-INF/LICENSE.txt")
        excludes.add("META-INF/license.txt")
        excludes.add("META-INF/NOTICE")
        excludes.add("META-INF/NOTICE.txt")
        excludes.add("META-INF/notice.txt")
        excludes.add("META-INF/ASL2.0")
        excludes.add("META-INF/gradle/incremental.annotation.processors")
    }
}

dependencies {
    /** ANDROID X */
    implementation(AndroidX.CORE)
    implementation(AndroidX.APP_COMPAT)
    implementation(AndroidX.ACTIVITY)
    implementation(AndroidXConstrainLayout.CONSTRAINT_LAYOUT)
    /** GOOGLE */
    implementation(Google.AR_CORE)
    /** COMPOSE */
    implementation(AndroidXLifecycle.LIFECYCLE_VIEWMODEL_KTX)
    implementation(AndroidCompose.ANDROID_COMPOSE_UI)
    implementation(AndroidCompose.ANDROID_COMPOSE_COMPILER)
    implementation(AndroidCompose.ANDROID_COMPOSE_MATERIAL)
    implementation(AndroidCompose.ACTIVITY_COMPOSE)
    implementation(AndroidCompose.ANDROID_COMPOSE_TOOLING)
    implementation(AndroidCompose.ANDROID_COMPOSE_RUNTIME)
    implementation(AndroidCompose.ANDROID_COMPOSE_LIVEDATA)
    implementation(AndroidCompose.ANDROID_COMPOSE_UI_UNIT)
    implementation(AndroidCompose.ANDROID_COMPOSE_ANIMATION)
    implementation(AndroidCompose.CONSTRAINT_COMPOSE)
    implementation(AndroidCompose.VIEW_MODEL_COMPOSE)
    implementation(AndroidCompose.ANDROID_COMPOSE_NAVIGATE)
    implementation(AndroidCompose.COMPOSE_LIFECYCLE_RUNTIME_COMPOSE)
    implementation(project(mapOf("path" to ":core:failureHandler")))
    implementation(project(mapOf("path" to ":core:failureHandler")))

    debugImplementation(AndroidCompose.COMPOSE_UI_MANIFEST)
    androidTestImplementation(AndroidCompose.COMPOSE_UI_JUNIT)
    debugImplementation(AndroidCompose.COMPOSE_UI_TOOLING)

    implementation(AndroidX.WINDOW)
    implementation(AndroidXNavigation.FRAGMENT_KTX)
    implementation(AndroidXNavigation.UI_KTX)

    implementation("com.google.accompanist:accompanist-systemuicontroller:0.28.0")

    /** COROUTINES */
    implementation(Coroutines.CORE)
    implementation(Coroutines.ANDROID)
    testImplementation(Coroutines.TEST)
    /** GLIDE*/
    implementation(Glide.LAND_SCAPIST)

    /**  DAGGER*/
    testImplementation(Hilt.HILT_TEST)
    kaptTest(Hilt.HILT_COMPILER)

    implementation(Hilt.DAGGER_ANDROID)
    implementation(Hilt.HILT_NAVIGATE)
    testAnnotationProcessor(Hilt.HILT_COMPILER)
    kapt(Hilt.HILT_COMPILER)

    androidTestImplementation(Hilt.HILT_TEST)
    kaptAndroidTest(Hilt.HILT_COMPILER)

    implementation(project(EnterpriseBusinessRules.ENTITIES))
    /** MODULE */
    implementation(project(SharedPool.HTTP_CLIENT))
    implementation(project(Core.CLEAN))
    implementation(project(SharedPool.NETWORK))
    implementation(project(Shared.MOVIE))
    implementation(project(Shared.TV_SHOW))
    implementation(project(FeaturePool.RESOURCES))
    implementation(project(InterfaceAdapter.DI))

    /** ROOM */
    implementation(Room.RUNTIME)
    implementation(Room.KTX)
    implementation(Room.COROUTINES)
    kapt(Room.KAPT_COMPILER)
    /** DEBUG */
    implementation(Debug.TIMBER)
    debugImplementation(AndroidCompose.ANDROID_COMPOSE_DEBUG)
    /** GOOGLE */
    implementation(Google.MATERIAL)
    /** COROUTINES **/
    implementation(Coroutines.ANDROID)
    /** TESTING */
    testImplementation(Junit.JUNIT)
    testImplementation(AndroidXTesting.ANDROID_CORE_TESTING)
    androidTestImplementation(AndroidXTesting.JUNIT)
    androidTestImplementation(AndroidXTesting.ESPRESSO_CORE)
    androidTestImplementation(AndroidCompose.ANDROID_COMPOSE_TEST)

    testImplementation(Mockk.MOCKK)

}