package com.araged.jetpack.presentation.common.preview

import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import com.araged.jetpack.presentation.home.model.Media
import com.argaed.entities.movies.Movie


class MovieResultPreview : PreviewParameterProvider<Media> {
    override val values = sequenceOf(Media(
        adult = false, posterPath = null, id = 0, title = ""
    ))
}