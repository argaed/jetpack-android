package com.araged.jetpack.presentation.home.model

import com.argaed.entities.movies.Movie
import com.argaed.entities.tv.ShowTv

data class Media(
    val adult: Boolean = false,
    val posterPath: String?,
    val id: Int,
    val title: String
) {

    companion object {

        fun fromShowTv(showTv: ShowTv) = Media(
            posterPath = showTv.posterPath,
            id = showTv.id,
            title = showTv.name
        )

        fun fromMovie(movie: Movie) =
            Media(
                adult = movie.adult,
                posterPath = movie.posterPath,
                id = movie.id,
                title = movie.title
            )
    }
}
