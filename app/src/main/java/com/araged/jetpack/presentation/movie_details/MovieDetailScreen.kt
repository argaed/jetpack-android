package com.araged.jetpack.presentation.movie_details

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.araged.clean.presentation.Status
import com.araged.jetpack.R
import com.araged.jetpack.presentation.common.components.FullScreenLoading
import com.argaed.entities.movies.MovieDetails
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.skydoves.landscapist.glide.GlideImage

@Composable
fun MovieDetailLoad(
    movieDetailsViewModel: MovieDetailsViewModel = hiltViewModel(),
    onBackClick: () -> Unit,
) {
    val lifecycleOwner: LifecycleOwner = LocalLifecycleOwner.current
    val uiState by movieDetailsViewModel.uiState.collectAsStateWithLifecycle(
        initialValue = MovieDetailsState(),
        lifecycle = lifecycleOwner.lifecycle,
        minActiveState = lifecycleOwner.lifecycle.currentState,
        context = lifecycleOwner.lifecycleScope.coroutineContext
    )

    LaunchedEffect(key1 = true){
        movieDetailsViewModel.loadMovieDetail(0)
    }
  
    uiState.let { movieDetailsState ->
        movieDetailsState.stateActivity.also { stateActivity ->
            FullScreenLoading(false)
            when (stateActivity) {
                is Status.Done -> MovieDetailScreen(uiState.movieDetails, onBackClick)
                is Status.Failed -> {}
                is Status.Loading -> FullScreenLoading(true)
                else -> FullScreenLoading(true)
            }
        }
    }


}

@Composable
fun MovieDetailScreen(
    movieDetails: MovieDetails,
    onBackClick: () -> Unit,
) {
    MovieDetailsContent(movieDetails)

}

@Composable
fun MovieDetailsContent(movieDetails: MovieDetails) {
    val urlBase = "https://www.themoviedb.org/t/p/w220_and_h330_face"
    val posterUrl = "${urlBase}${movieDetails.backdropPath}"
    Column(
        modifier = Modifier.padding(16.dp)
    ) {
        GlideImage(
            modifier = Modifier
                .fillMaxWidth()
                .height(300.dp)
                .clip(RoundedCornerShape(16.dp)),
            imageModel = posterUrl,
            loading = {
                ConstraintLayout(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(300.dp)
                ) {
                    val indicator = createRef()
                    CircularProgressIndicator(
                        modifier = Modifier.constrainAs(indicator) {
                            top.linkTo(parent.top)
                            bottom.linkTo(parent.bottom)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        }
                    )
                }
            },
            requestOptions = {
                RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
            },
            contentScale = ContentScale.Crop,
            // shows an error text message when request failed.
            failure = {
                Column(verticalArrangement = Arrangement.Center) {
                    Image(
                        painter = painterResource(id = R.drawable.baseline_warning),
                        contentDescription = "warning"
                    )
                }
                //Text(text = "image request failed.")
            },
            previewPlaceholder = R.drawable.poster
        )
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            text = movieDetails.title,
            style = MaterialTheme.typography.h5
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            text = movieDetails.releaseDate ?: "",
            style = MaterialTheme.typography.subtitle1
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            text = movieDetails.overview,
            style = MaterialTheme.typography.body1
        )
    }
}
