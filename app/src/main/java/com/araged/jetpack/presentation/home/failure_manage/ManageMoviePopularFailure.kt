package com.araged.jetpack.presentation.home.failure_manage

import android.content.Context
import android.widget.Toast
import com.araged.jetpack.presentation.common.components.getCommonFailureMessage
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularFailure

fun Context.manageMoviePopularFailure(
    failure: GetMoviePopularFailure,
){
    getCommonFailureMessage(this, failure)
}

fun getCommonFailureMessage(
    context: Context,
    failure: GetMoviePopularFailure
) {
    val message: String = context.getCommonFailureMessage(failure)
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
}