package com.araged.jetpack.presentation.movie_details

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument

const val DestinationOneArg = "arg"
private const val DestinationMovieDetailsRoot = "MovieDetails"
private const val DestinationMovieDetailsRoute = "$DestinationMovieDetailsRoot/{$DestinationOneArg}"

fun buildMovieDetailsRoute(argument: String) =
    DestinationMovieDetailsRoute.replace("{$DestinationOneArg}", argument)

fun NavGraphBuilder.addMovieDetailsDestination(
    navController: NavController
) {
    composable(
        route = DestinationMovieDetailsRoute,
        arguments = listOf(navArgument(DestinationOneArg) { type = NavType.IntType })
    ) {
        MovieDetailLoad(
            onBackClick = { navController.popBackStack() },
        )
    }
}
