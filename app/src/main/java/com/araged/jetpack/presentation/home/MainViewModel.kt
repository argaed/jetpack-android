package com.araged.jetpack.presentation.home

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.runtime.toMutableStateList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.araged.clean.*
import com.araged.clean.presentation.Status
import com.araged.jetpack.di.components.IoDispatcher
import com.araged.jetpack.presentation.home.model.Media
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularParams
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularUseCase
import com.araged.movie.domian.use_case.get_movie_popular.GetTvShowPopularParams
import com.araged.movie.domian.use_case.get_movie_rated.GetMovieRatedParams
import com.araged.movie.domian.use_case.get_movie_rated.GetMovieRatedUseCase
import com.araged.movie.domian.use_case.get_movie_upcoming.GetMovieUpcomingParams
import com.araged.movie.domian.use_case.get_movie_upcoming.GetMovieUpcomingUseCase
import com.araged.movie.domian.use_case.get_movie_upcoming.GetTvShowLatestParams
import com.araged.show_tv.domian.use_case.get_tv_show_latest.GetTvShowLatestUseCase
import com.araged.show_tv.domian.use_case.get_tv_show_popular.GetTvShowPopularUseCase
import com.araged.show_tv.domian.use_case.get_tv_show_rated.GetTvShowRatedParams
import com.araged.show_tv.domian.use_case.get_tv_show_rated.GetTvShowRatedUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

typealias LoadHomeStatus = Status<Failure, UseCase.None>

data class MoviesState(
    /*Movie*/
    val moviesPopular: SnapshotStateList<Media> = mutableStateListOf(),
    val moviesRated: SnapshotStateList<Media> = mutableStateListOf(),
    val moviesUpcoming: SnapshotStateList<Media> = mutableStateListOf(),
    /*Tv*/
    val tvShowPopular: SnapshotStateList<Media> = mutableStateListOf(),
    val tvShowRated: SnapshotStateList<Media> = mutableStateListOf(),
    val tvShowLatest: SnapshotStateList<Media> = mutableStateListOf(),
    /*state*/
    val stateActivity: LoadHomeStatus = Status.Loading()
)


@HiltViewModel
class MainViewModel
@Inject constructor(
    private val getMovieRatedUseCase: GetMovieRatedUseCase,
    private val getMoviePopularUseCase: GetMoviePopularUseCase,
    private val getMovieUpcomingUseCase: GetMovieUpcomingUseCase,
    private val getTvShowLatestUseCase: GetTvShowLatestUseCase,
    private val getTvShowPopularUseCase: GetTvShowPopularUseCase,
    private val getTvShowRatedUseCase: GetTvShowRatedUseCase,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : ViewModel() {

    private val _uiState = MutableStateFlow(MoviesState())
    val uiState: StateFlow<MoviesState> = _uiState.asStateFlow()

    fun loadHome() = viewModelScope.launch {
        withContext(ioDispatcher) {
            getMoviePopularUseCase.run(params = GetMoviePopularParams)
                .onLeft { failure -> showError(failure) }
                .onRight { response ->
                    _uiState.update { currentState ->
                        currentState.copy(
                            moviesPopular = response.movies.map { movie -> Media.fromMovie(movie) }.toMutableStateList()
                        )
                    }
                }
        }

        withContext(ioDispatcher) {
            getMovieRatedUseCase.run(params = GetMovieRatedParams)
                .onLeft { failure ->
                    showError(failure)
                }.onRight { response ->
                    _uiState.update { currentState ->
                        currentState.copy(
                            moviesRated = response.movies.map { movie -> Media.fromMovie(movie) }.toMutableStateList()
                        )
                    }
                }
        }

        withContext(ioDispatcher) {
            getMovieUpcomingUseCase.run(
                params = GetMovieUpcomingParams
            ).onLeft { failure ->
                showError(failure)
            }.onRight { response ->
                _uiState.update { currentState ->
                    currentState.copy(
                        moviesUpcoming = response.movies.map { movie -> Media.fromMovie(movie) }.toMutableStateList()
                    )
                }
            }
        }

        withContext(ioDispatcher) {
            getTvShowPopularUseCase.run(
                params = GetTvShowPopularParams
            ).onLeft { failure ->
                showError(failure)
            }.onRight { response ->
                _uiState.update { currentState ->
                    currentState.copy(
                        tvShowPopular = response.tvShows.map { tvShow -> Media.fromShowTv(tvShow) }.toMutableStateList()
                    )
                }
            }
        }
        withContext(ioDispatcher) {
            getTvShowRatedUseCase.run(params = GetTvShowRatedParams).onLeft { failure ->
                showError(failure)
            }.onRight { response ->
                _uiState.update { currentState ->
                    currentState.copy(
                        tvShowRated = response.tvShows.map { tvShow -> Media.fromShowTv(tvShow) }.toMutableStateList()
                    )
                }
            }
        }
        withContext(ioDispatcher) {
            getTvShowLatestUseCase.run(params = GetTvShowLatestParams).onLeft { failure ->
                showError(failure)
            }.onRight { response ->
                _uiState.update { currentState ->
                    currentState.copy(
                        tvShowLatest = response.tvShows.map { tvShow -> Media.fromShowTv(tvShow) }.toMutableStateList()
                    )
                }
            }
        }

        _uiState.update { currentState ->
            currentState.copy(
                stateActivity = Status.Done(UseCase.None)
            )
        }

    }

    private fun showError(failure: Failure) {
        _uiState.update { currentState ->
            currentState.copy(
                stateActivity = Status.Failed(failure)
            )
        }
    }

}