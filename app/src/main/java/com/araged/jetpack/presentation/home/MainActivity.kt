package com.araged.jetpack.presentation.home

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import com.araged.jetpack.theme.JetpackTheme
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import dagger.hilt.android.AndroidEntryPoint
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import com.araged.jetpack.presentation.movie_details.addMovieDetailsDestination

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            JetpackTheme {

                val scaffoldState = rememberScaffoldState()

                val systemUiController = rememberSystemUiController()

                val darkIcons = MaterialTheme.colors.isLight

                SideEffect {
                    systemUiController.setSystemBarsColor(Color.Transparent, darkIcons = darkIcons)
                }
                val navController = rememberNavController()

                Scaffold(
                    scaffoldState = scaffoldState, modifier = Modifier.fillMaxSize(),
                    content = {

                        NavHost(
                            navController,
                            startDestination = DestinationOneRoute,
                        ) {
                            addHomeDestination(navController)
                            addMovieDetailsDestination(navController)
                        }
                    }
                )
            }
        }
    }

}
