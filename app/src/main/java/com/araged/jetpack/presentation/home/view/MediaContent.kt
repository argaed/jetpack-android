package com.araged.jetpack.presentation.home.view

import android.annotation.SuppressLint
import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Devices.NEXUS_7
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.araged.clean.Failure
import com.araged.clean.presentation.Status
import com.araged.jetpack.R
import com.araged.jetpack.presentation.common.components.BindCoverMedia
import com.araged.jetpack.presentation.common.components.FullScreenLoading
import com.araged.jetpack.presentation.common.preview.MovieProvider
import com.araged.jetpack.presentation.home.MainViewModel
import com.araged.jetpack.presentation.home.MoviesState
import com.araged.jetpack.presentation.home.failure_manage.manageMoviePopularFailure
import com.araged.jetpack.presentation.home.model.Media
import com.araged.jetpack.presentation.home.model.MediaCategory
import com.araged.jetpack.theme.JetpackTheme
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularFailure
import kotlinx.coroutines.launch


@Composable
fun LoadHome(
    onNavigate: (String) -> Unit,
    mainViewModel: MainViewModel = hiltViewModel(),
) {
    var firstCall by rememberSaveable { mutableStateOf(false) }
    val scope = rememberCoroutineScope()
    val uiState by mainViewModel.uiState.collectAsStateWithLifecycle()

    LayoutScreen(uiState, onNavigate)
    LaunchedEffect(key1 = Unit) {
        if (!firstCall) {
            scope.launch { mainViewModel.loadHome() }
            firstCall = true
        }

    }

}


@Composable
fun LayoutScreen(uiState: MoviesState, onNavigate: (String) -> Unit) {
    val context = LocalContext.current
    uiState.let { movieState ->
        movieState.stateActivity.also { stateActivity ->
            FullScreenLoading(false)
            when (stateActivity) {
                is Status.Loading -> FullScreenLoading(true)
                is Status.Failed -> manageLoadHomeFailure(stateActivity.failure, context)
                is Status.Done -> MovieScreen(movieState, onNavigate)
            }
        }
    }
}

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
private fun MovieScreen(moviesState: MoviesState, onNavigate: (String) -> Unit) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text("Movie")
                }
            )
        },
        content = {
            MovieScreenContent(moviesState = moviesState, onNavigate = onNavigate)
        }
    )

}

@Composable
private fun MovieScreenContent(moviesState: MoviesState, onNavigate: (String) -> Unit) {
    LazyColumn(
        contentPadding = PaddingValues(horizontal = 10.dp, vertical = 10.dp)
    ) {
        item {
            moviesState.let { media ->
                MediaSection(
                    MediaCategory(
                        stringResource(R.string.movie_popular),
                        media.moviesPopular
                    ), onNavigate = {
                        onNavigate(it)
                    }
                )
                MediaSection(
                    MediaCategory(
                        stringResource(R.string.movie_rated),
                        media.moviesRated
                    ), onNavigate = {
                        onNavigate(it)
                    }
                )
                MediaSection(
                    MediaCategory(
                        stringResource(R.string.movie_upcoming),
                        media.moviesUpcoming
                    ), onNavigate = {
                        onNavigate(it)
                    }
                )
                MediaSection(
                    MediaCategory(
                        stringResource(R.string.tv_show_popular),
                        media.tvShowPopular
                    ), onNavigate = {
                        onNavigate(it)
                    }
                )
                MediaSection(
                    MediaCategory(
                        stringResource(R.string.tv_show_rated),
                        media.tvShowRated
                    ), onNavigate = {
                        onNavigate(it)
                    }
                )
                MediaSection(
                    MediaCategory(
                        stringResource(R.string.tv_show_latest),
                        media.tvShowLatest
                    ), onNavigate = {
                        onNavigate(it)
                    }
                )
            }
        }
    }
}

@Composable
private fun MediaSection(movieCategory: MediaCategory, onNavigate: (String) -> Unit) {
    if (movieCategory.medias.isNotEmpty()) {
        Text(
        text = movieCategory.nameCategoryHome,
        modifier = Modifier,
        fontSize = 20.sp
    )
    LazyRow(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.spacedBy(10.dp),
        contentPadding = PaddingValues(horizontal = 0.dp, vertical = 8.dp)
    ) { items(movieCategory.medias) { MediaLayout(it, onNavigate) } }}
}

@Composable
private fun MediaLayout(media: Media, onNavigate: (String) -> Unit) {
    Column(
        modifier = Modifier
            .wrapContentHeight()
            .width(150.dp)
            .clip(shape = MaterialTheme.shapes.medium)
            .clickable {
                onNavigate(media.id.toString())
            }
    ) {
        BindCoverMedia(media)
        Spacer(modifier = Modifier.padding(top = 5.dp))
        Text(text = media.title, modifier = Modifier, maxLines = 1)
    }
}

private fun manageLoadHomeFailure(failure: Failure, context: Context) =

    failure.let {
        when (it) {
            is GetMoviePopularFailure -> {
                context.manageMoviePopularFailure(it)
            }
            /* is GetMovieRatedFailure ->{
                 context.getCommonFailureMessage(failure)
             }
             is GetMovieUpcomingFailure ->{
                 context.getCommonFailureMessage(failure as GetMovieUpcomingFailure)
             }
             is GetTvShowLatestFailure ->{
                 context.getCommonFailureMessage(failure as GetTvShowLatestFailure)
             }
             is GetTvShowPopularFailure ->{
                 context.getCommonFailureMessage(failure as GetTvShowPopularFailure)
             }
             is GetTvShowRatedFailure ->{
                 context.getCommonFailureMessage(failure as GetTvShowRatedFailure)
             }*/

        }
        Toast.makeText(context, "Error", Toast.LENGTH_LONG).show()
    }


@Preview(device = NEXUS_7, showBackground = true)
@Composable
fun ComposablePreview(@PreviewParameter(MovieProvider::class) moviesState: MoviesState) {
    JetpackTheme {
        LayoutScreen(uiState = moviesState, onNavigate = {})
    }
}
