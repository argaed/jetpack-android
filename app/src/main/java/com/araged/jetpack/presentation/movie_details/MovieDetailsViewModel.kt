package com.araged.jetpack.presentation.movie_details

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.araged.clean.Failure
import com.araged.clean.onLeft
import com.araged.clean.onRight
import com.araged.clean.presentation.Status
import com.araged.jetpack.di.components.IoDispatcher
import com.araged.movie.domian.use_case.get_movie_details.GetMovieDetailsParams
import com.araged.movie.domian.use_case.get_movie_details.GetMovieDetailsUseCase
import com.argaed.entities.movies.MovieDetails
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

typealias MovieDetailsStatus = Status<Failure, MovieDetails>


data class MovieDetailsState(
    val movieDetails: MovieDetails = MovieDetails(),
    val stateActivity: MovieDetailsStatus = Status.Loading(),
)

@HiltViewModel
class MovieDetailsViewModel @Inject constructor(
    private val getMovieDetailsUseCase: GetMovieDetailsUseCase,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
  private val savedStateHandle: SavedStateHandle,
) : ViewModel() {

    init {
        val argument = savedStateHandle.get<Int>(DestinationOneArg) ?: 0
       // loadMovieDetail(argument)
    }


    private val _uiState = MutableStateFlow(MovieDetailsState())
    val uiState: Flow<MovieDetailsState> = _uiState


     fun loadMovieDetail(id: Int) = viewModelScope.launch {
        withContext(ioDispatcher) {
            val idd = savedStateHandle.get<Int>(DestinationOneArg) ?: 0
            getMovieDetailsUseCase.run(params = GetMovieDetailsParams(idd))
                .onLeft { failure -> showError(failure) }
                .onRight { response ->
                    _uiState.update { currentState ->
                        currentState.copy(
                            movieDetails = response.movieDetails,
                            stateActivity = Status.Done(response.movieDetails)
                        )
                    }
                }
        }

    }

    private fun showError(failure: Failure) {
        _uiState.update { currentState ->
            currentState.copy(
                stateActivity = Status.Failed(failure)
            )
        }
    }


}