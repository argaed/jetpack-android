package com.araged.jetpack.presentation.common.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.araged.jetpack.R
import com.araged.jetpack.presentation.common.preview.MovieResultPreview
import com.araged.jetpack.presentation.home.model.Media
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.skydoves.landscapist.CircularReveal
import com.skydoves.landscapist.glide.GlideImage

@Composable
fun BindCoverMedia(media: Media) {
    val urlBase = "https://www.themoviedb.org/t/p/w220_and_h330_face"
    val posterUrl = "${urlBase}${media.posterPath}"
    GlideImage(
        modifier = Modifier
            .size(height = 200.dp, width = 150.dp),
        imageModel = posterUrl,
        loading = {
            ConstraintLayout(
                modifier = Modifier
                    .heightIn(min = 200.dp)
                    .fillMaxWidth()
            ) {
                val indicator = createRef()
                CircularProgressIndicator(
                    modifier = Modifier.constrainAs(indicator) {
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }
                )
            }
        },
        requestOptions = {
            RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
        },
        contentScale = ContentScale.Crop,
        circularReveal = CircularReveal(duration = 2000),
        // shows an error text message when request failed.
        failure = {
            Column(verticalArrangement = Arrangement.Center) {
                Image(
                    painter = painterResource(id = R.drawable.baseline_warning),
                    contentDescription = "warning"
                )
            }
            //Text(text = "image request failed.")
        },
        previewPlaceholder = R.drawable.poster
    )
}

@Preview
@Composable
fun PreviewGlideMovie(
    @PreviewParameter(MovieResultPreview::class)
    media: Media
) {
    BindCoverMedia(media)
}