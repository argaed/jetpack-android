package com.araged.jetpack.presentation.common.preview

import androidx.compose.runtime.toMutableStateList
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import com.araged.clean.UseCase
import com.araged.clean.presentation.Status
import com.araged.jetpack.presentation.home.MoviesState
import com.araged.jetpack.presentation.home.model.Media
import com.argaed.entities.movies.Movie
import com.argaed.entities.tv.ShowTv

class MovieProvider : PreviewParameterProvider<MoviesState> {
    override val values = sequenceOf(getData())

    private fun getData(): MoviesState {
        val responseMovie = listOf(
            Media(
                adult = false,
                id = 646389,
                posterPath = "/qi9r5xBgcc9KTxlOLjssEbDgO0J.jpg",
                title = "Plane"
            )
        )
        val responseTv = listOf(
            Media(
                id = 646389,
                title = "The last of us",
                posterPath = "/qi9r5xBgcc9KTxlOLjssEbDgO0J.jpg",
            )
        )
        return MoviesState(
            moviesPopular = responseMovie.toMutableStateList(),
            moviesRated = responseMovie.toMutableStateList(),
            moviesUpcoming = responseMovie.toMutableStateList(),
            /*tv*/
            tvShowRated = responseTv.toMutableStateList(),
            tvShowPopular = responseTv.toMutableStateList(),
            tvShowLatest = responseTv.toMutableStateList(),
            stateActivity = Status.Done(UseCase.None)
        )
    }
}