package com.araged.jetpack.presentation.common.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview

/**
 * Full screen circular progress indicator
 */
@Composable
fun FullScreenLoading(isDisplayed: Boolean) {
    if (isDisplayed) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .wrapContentSize(Alignment.Center),

            ) {
            CircularProgressIndicator()
        }
    }

}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
        FullScreenLoading(true)
}

