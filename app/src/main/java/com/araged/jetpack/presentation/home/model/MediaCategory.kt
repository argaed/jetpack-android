package com.araged.jetpack.presentation.home.model

data class MediaCategory(
    val nameCategoryHome: String,
    val medias: List<Media>
)
