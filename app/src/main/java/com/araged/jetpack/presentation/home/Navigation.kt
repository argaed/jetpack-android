package com.araged.jetpack.presentation.home

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.araged.jetpack.presentation.home.view.LoadHome
import com.araged.jetpack.presentation.movie_details.buildMovieDetailsRoute

const val DestinationOneRoute = "Home"

fun NavGraphBuilder.addHomeDestination(
    navController: NavController,
) {
    composable(
        route = DestinationOneRoute,
    ) {
        LoadHome(onNavigate = { argument ->
            navController.navigate(buildMovieDetailsRoute(argument))
        })
    }
}
