package com.araged.jetpack.presentation.common.preview

import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import com.argaed.entities.tv.ShowTv

class ResultTvShowProvider: PreviewParameterProvider<ShowTv>  {
    override val values = sequenceOf(ShowTv(
        backdropPath = null,
        firstAirDate = "",
        id = 0,
        name = "Tv Name",
        originalLanguage = "",
        originalName = "",
        overview = "",
        popularity = 0.0,
        posterPath = "",
        voteAverage = 0.0,
        voteCount = 0,
        category = ""
    ))
}