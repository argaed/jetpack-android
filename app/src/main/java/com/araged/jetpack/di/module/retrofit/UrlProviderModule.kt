package com.araged.jetpack.di.module.retrofit

import com.araged.httpclient.retrofit.Authorization
import com.araged.httpclient.retrofit.UrlProvider
import com.araged.jetpack.BuildConfig
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class UrlProviderModule {

    @Provides
    fun provideUrlProvider():
            UrlProvider = UrlProvider(BuildConfig.API_BASE_URL)

    @Provides
    fun provideAuthorization() = Authorization(BuildConfig.Authorization)
}