package com.araged.jetpack

import android.app.Application
import com.araged.network.presentation.initNetworkConnectionBroadcastReceiver
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class JetpackApp: Application() {

    override fun onCreate() {
        super.onCreate()
        initNetworkConnectionBroadcastReceiver()
        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
    }

}