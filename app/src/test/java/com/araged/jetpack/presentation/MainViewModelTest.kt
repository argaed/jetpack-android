package com.araged.jetpack.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.araged.clean.Either
import com.araged.clean.presentation.Status
import com.araged.jetpack.di.components.IoDispatcher
import com.araged.jetpack.presentation.home.MainViewModel
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularParams
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularResponse
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularUseCase
import com.araged.movie.domian.use_case.get_movie_rated.GetMovieRatedParams
import com.araged.movie.domian.use_case.get_movie_rated.GetMovieRatedResponse
import com.araged.movie.domian.use_case.get_movie_rated.GetMovieRatedUseCase
import com.araged.movie.domian.use_case.get_movie_upcoming.GetMovieUpcomingParams
import com.araged.movie.domian.use_case.get_movie_upcoming.GetMovieUpcomingResponse
import com.araged.movie.domian.use_case.get_movie_upcoming.GetMovieUpcomingUseCase
import com.araged.show_tv.domian.use_case.get_tv_show_latest.GetTvShowLatestUseCase
import com.araged.show_tv.domian.use_case.get_tv_show_popular.GetTvShowPopularUseCase
import com.araged.show_tv.domian.use_case.get_tv_show_rated.GetTvShowRatedUseCase
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import com.argaed.entities.movies.Movie
@ExperimentalCoroutinesApi
internal class MainViewModelTest {

    @RelaxedMockK
    private lateinit var getMovieRatedUseCase: GetMovieRatedUseCase

    @RelaxedMockK
    private lateinit var getMoviePopularUseCase: GetMoviePopularUseCase

    @RelaxedMockK
    private lateinit var getMovieUpcomingUseCase: GetMovieUpcomingUseCase

    @RelaxedMockK
    private lateinit var getTvShowLatestUseCase: GetTvShowLatestUseCase

    @RelaxedMockK
    private lateinit var getTvShowPopularUseCase: GetTvShowPopularUseCase

    @RelaxedMockK
    private lateinit var getTvShowRatedUseCase: GetTvShowRatedUseCase

    @IoDispatcher
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    @get:Rule
    var rule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var noticesViewModel: MainViewModel

    @Before
    fun onBefore() {
        MockKAnnotations.init(this)
        noticesViewModel = MainViewModel(
            getMovieRatedUseCase = getMovieRatedUseCase,
            getMoviePopularUseCase = getMoviePopularUseCase,
            getMovieUpcomingUseCase = getMovieUpcomingUseCase,
            getTvShowLatestUseCase = getTvShowLatestUseCase,
            getTvShowPopularUseCase = getTvShowPopularUseCase,
            getTvShowRatedUseCase = getTvShowRatedUseCase,
            ioDispatcher = ioDispatcher
        )
        Dispatchers.setMain(Dispatchers.Default)
    }

    @After
    fun onAfter() {
        Dispatchers.resetMain()
    }


    @Test
    fun `happy patch home`() = runTest {
        //given every when you require this
        coEvery { getMoviePopularUseCase.run(GetMoviePopularParams) } returns
                Either.Right(
                    GetMoviePopularResponse(
                        listOf(
                            Movie(
                                adult = false,
                                backdropPath = "",
                                id = 0,
                                originalLanguage = "",
                                originalTitle = "",
                                overview = "",
                                popularity = 0.0,
                                posterPath = "",
                                releaseDate = "",
                                title = "",
                                video = false,
                                voteAverage = 0.0,
                                voteCount = 0,
                                category = ""
                            )
                        )
                    )
                )

        coEvery { getMovieRatedUseCase.run(params = GetMovieRatedParams) } returns Either.Right(
            GetMovieRatedResponse(
                emptyList()
            )
        )
        coEvery { getMovieUpcomingUseCase.run(params = GetMovieUpcomingParams) } returns Either.Right(
           GetMovieUpcomingResponse(
                emptyList()
            )
        )

        coEvery { getMovieUpcomingUseCase.run(GetMovieUpcomingParams) } returns Either.Right(
            GetMovieUpcomingResponse(emptyList())
        )
        //when
        noticesViewModel.loadHome()

        //Then
        when (noticesViewModel.uiState.value.stateActivity) {
            is Status.Done -> {
                assert(noticesViewModel.uiState.value.moviesPopular.size == 1)
                assert(noticesViewModel.uiState.value.moviesRated.size == 0)
            }
            is Status.Failed -> {
            }
            is Status.Loading -> {}
        }
        coVerify(exactly = 1) { getMoviePopularUseCase.run(GetMoviePopularParams) }
        coVerify(exactly = 1) { getMovieRatedUseCase.run(GetMovieRatedParams) }
        coVerify(exactly = 1) { getMovieUpcomingUseCase.run(GetMovieUpcomingParams) }
    }


}