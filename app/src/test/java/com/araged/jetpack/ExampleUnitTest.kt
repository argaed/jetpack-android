package com.araged.jetpack

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */

data class Shoe(
    var size: Int = 38,
    var color: String = "Negro",
    var stock: Int = 10
)

class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        val newShoe = Shoe()

        val shoeDetail = newShoe.apply {
            size = 42
            color = "Rojo"
            stock = 25
        }.toString()
        val lados: Int = 10
        var ladosTexto: String = ""
        ladosTexto = lados.toString()
        for (x in 10 downTo 1) {
            print(x)
        }
        print(shoeDetail)
        assertEquals(4, 2 + 2)
    }
}


fun Int?.isEven(): Boolean {
    return if (this != null) {
        this % 2 == 0
    } else throw RuntimeException("Number cannot be null")
}

class Test() {
    @Test
    fun isEvenTest(actualNumber : Int?) {
        if (actualNumber != null) {
            val expectedResult = actualNumber  % 2 == 0
            assertEquals(expectedResult, actualNumber.isEven())
        }
    }

}
