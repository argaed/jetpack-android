package com.araged.show_tv.domian.use_case.get_tv_show_rated

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.araged.clean.Either
import com.araged.movie.domian.MovieRepository
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularParams
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularResponse
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularUseCase
import com.araged.movie.domian.use_case.get_movie_rated.GetMovieRatedParams
import com.araged.movie.domian.use_case.get_movie_rated.GetMovieRatedResponse
import com.araged.movie.domian.use_case.get_movie_rated.GetMovieRatedUseCase
import com.araged.movie.domian.use_case.get_movie_upcoming.GetMovieUpcomingParams
import com.araged.movie.domian.use_case.get_movie_upcoming.GetMovieUpcomingResponse
import com.araged.movie.domian.use_case.get_movie_upcoming.GetMovieUpcomingUseCase
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test


internal class GetTvShowRatedUseCaseTest{

    @RelaxedMockK
    private lateinit var studentRepository: MovieRepository

    private lateinit var getMovieRatedUseCase: GetMovieRatedUseCase
    private lateinit var getMoviePopularUseCase: GetMoviePopularUseCase
    private lateinit var getMovieUpcomingUseCase: GetMovieUpcomingUseCase

    @get:Rule
    var rule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun onBefore(){
        MockKAnnotations.init(this)
        getMovieRatedUseCase = GetMovieRatedUseCase(studentRepository)
        getMoviePopularUseCase = GetMoviePopularUseCase(studentRepository)
        getMovieUpcomingUseCase = GetMovieUpcomingUseCase(studentRepository)
    }

    @Test
    fun fistTest() = runBlocking{
        coEvery { studentRepository.getMoviePopular() } returns Either.Right(GetMoviePopularResponse(emptyList()))
        coEvery { studentRepository.getMovieRated() } returns Either.Right(GetMovieRatedResponse(emptyList()))
        coEvery { studentRepository.getMovieUpcoming() } returns Either.Right(
            GetMovieUpcomingResponse(emptyList())
        )
        //when
        getMovieRatedUseCase.run(GetMovieRatedParams)
        getMoviePopularUseCase.run(GetMoviePopularParams)
        getMovieUpcomingUseCase.run(GetMovieUpcomingParams)
        main()

        coVerify(exactly = 1){studentRepository.getMoviePopular() }
        coVerify(exactly = 1){studentRepository.getMovieRated() }
        coVerify(exactly = 1){studentRepository.getMovieUpcoming() }
    }

    fun operacionMatematica(a: Int, b: Int, operador: (Int, Int) -> Int): Int {
        return operador(a, b)
    }

    fun suma(a: Int, b: Int): Int {
        return a + b
    }

    fun resta(a: Int, b: Int): Int {
        return a - b
    }

    fun multiplicacion(a: Int, b: Int): Int {
        return a * b
    }

    fun division(a: Int, b: Int): Int {
        return a / b
    }

    fun main() {
        val resultadoSuma = operacionMatematica(5, 3, ::suma)
        println("Resultado de la suma: $resultadoSuma")

        val resultadoResta = operacionMatematica(8, 2, ::resta)
        println("Resultado de la resta: $resultadoResta")

        val resultadoMultiplicacion = operacionMatematica(4, 6, ::multiplicacion)
        println("Resultado de la multiplicación: $resultadoMultiplicacion")

        val resultadoDivision = operacionMatematica(10, 2, ::division)
        println("Resultado de la división: $resultadoDivision")
    }



}