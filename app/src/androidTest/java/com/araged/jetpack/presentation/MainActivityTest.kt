package com.araged.jetpack.presentation

import androidx.activity.compose.setContent
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.araged.jetpack.presentation.home.MainActivity
import com.araged.jetpack.presentation.home.view.LoadHome
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest{

    @get:Rule(order = 1)
    var hiltTestRule = HiltAndroidRule(this)

    @get:Rule(order = 2)
    var composeTestRule = createAndroidComposeRule<MainActivity>()

    @Before
    fun init() {
        hiltTestRule.inject()
    }

    @Test
    fun itemsAddedToScreen() {
        composeTestRule.onRoot().printToLog("TAG")
        composeTestRule.activity.setContent {
            LoadHome({})
        }

        composeTestRule.onNodeWithText("Movie Popular")
        composeTestRule.onNodeWithText("Movie Top Rated")
        composeTestRule.onNodeWithText("Plane")
        composeTestRule.onNodeWithText("ShowTv")

    }
}