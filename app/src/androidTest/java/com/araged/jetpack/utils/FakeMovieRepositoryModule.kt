package com.araged.jetpack.utils

import com.araged.clean.Either
import com.araged.movie.domian.MovieRepository
import com.araged.movie.domian.use_case.get_movie_details.GetMovieDetailsFailure
import com.araged.movie.domian.use_case.get_movie_details.GetMovieDetailsResponse
import com.araged.movie.domian.use_case.get_movie_details.GetMovieDetailsUseCase
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularFailure
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularResponse
import com.araged.movie.domian.use_case.get_movie_popular.GetMoviePopularUseCase
import com.araged.movie.domian.use_case.get_movie_rated.GetMovieRatedFailure
import com.araged.movie.domian.use_case.get_movie_rated.GetMovieRatedResponse
import com.araged.movie.domian.use_case.get_movie_rated.GetMovieRatedUseCase
import com.araged.movie.domian.use_case.get_movie_upcoming.GetMovieUpcomingFailure
import com.araged.movie.domian.use_case.get_movie_upcoming.GetMovieUpcomingResponse
import com.araged.movie.domian.use_case.get_movie_upcoming.GetMovieUpcomingUseCase
import com.example.di.movie.MovieModule
import dagger.Module
import dagger.Provides
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import javax.inject.Singleton
import com.argaed.entities.movies.Movie
import com.argaed.entities.movies.MovieDetails

@Module
@TestInstallIn(
    components = [SingletonComponent::class],
    replaces = [MovieModule::class],

    )
class FakeMovieRepositoryModule {

    @Provides
    @Singleton
    fun provideMovieRepository(): MovieRepository = object : MovieRepository {
        val response = listOf(
            Movie(
                adult = false,
                backdropPath = "/irwQcdjwtjLnaA0iErabab9PrmG.jpg",
                id = 646389,
                originalLanguage = "en",
                originalTitle = "Plane",
                overview = "After a heroic job of successfully landing his storm-damaged aircraft in a war zone, a fearless pilot finds himself between the agendas of multiple militias planning to take the plane and its passengers hostage.",
                popularity = 2699.401,
                posterPath = "/qi9r5xBgcc9KTxlOLjssEbDgO0J.jpg",
                releaseDate = "2023-01-12",
                title = "Plane",
                video = false,
                voteAverage = 6.9,
                voteCount = 668
            )
        )

        override suspend fun getMoviePopular():
                Either<GetMoviePopularFailure, GetMoviePopularResponse> = try {
            Either.Right(GetMoviePopularResponse(response))
        } catch (e: Exception) {

            Either.Left(GetMoviePopularFailure.UserTimeOut)
        }

        override suspend fun getMovieRated():
                Either<GetMovieRatedFailure, GetMovieRatedResponse> = try {

            Either.Right(GetMovieRatedResponse(response))
        } catch (e: Exception) {
            Either.Left(GetMovieRatedFailure.UserTimeOut)
        }

        override suspend fun getMovieUpcoming():
                Either<GetMovieUpcomingFailure, GetMovieUpcomingResponse> =
            try {
                Either.Right(GetMovieUpcomingResponse(response))
            } catch (e: Exception) {

                Either.Left(GetMovieUpcomingFailure.UserDisabled)
            }

        override suspend fun getMovieDetails(movieId: Int): Either<GetMovieDetailsFailure, GetMovieDetailsResponse> =
            try {
                val details = MovieDetails(
                    adult = false,
                    backdropPath = "",
                    budget = 0,
                    homepage = "",
                    id = 0,
                    imdbId = "",
                    originalLanguage = "",
                    originalTitle = "",
                    overview = "",
                    popularity = 0.0,
                    posterPath = "",
                    releaseDate = "",
                    revenue = 0,
                    runtime = 0,
                    status = "",
                    tagline = "",
                    title = "",
                    video = false,
                    voteAverage = 0.0,
                    voteCount = 0
                )
                Either.Right(GetMovieDetailsResponse(details))
            } catch (e: Exception) {

                Either.Left(GetMovieDetailsFailure.UserTimeOut)
            }
    }

    /** USE CASE*/
    @Provides
    @Singleton
    fun provideGetMoviePopularUseCase(
        movieRepository: MovieRepository
    ) = GetMoviePopularUseCase(movieRepository)

    @Provides
    @Singleton
    fun provideGetMovieRatedUseCase(
        movieRepository: MovieRepository
    ) = GetMovieRatedUseCase(movieRepository)

    @Provides
    @Singleton
    fun provideGetMovieUpcomingUseCase(
        movieRepository: MovieRepository
    ) = GetMovieUpcomingUseCase(movieRepository)

    @Provides
    @Singleton
    fun provideGetMovieDetailsUseCase(
        movieRepository: MovieRepository
    ) = GetMovieDetailsUseCase(movieRepository)

}
