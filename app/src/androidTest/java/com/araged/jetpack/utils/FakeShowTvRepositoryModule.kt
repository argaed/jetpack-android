package com.araged.jetpack.utils

import com.araged.clean.Either
import com.araged.show_tv.domian.use_case.get_tv_show_popular.GetTvShowPopularFailure
import com.araged.show_tv.domian.use_case.get_tv_show_rated.GetTvShowRatedFailure
import com.araged.show_tv.domian.TvShowRepository
import com.araged.show_tv.domian.use_case.get_tv_show_details.GetTvShowDetailsFailure
import com.araged.show_tv.domian.use_case.get_tv_show_details.GetTvShowDetailsResponse
import com.araged.show_tv.domian.use_case.get_tv_show_details.GetTvShowDetailsUseCase
import com.araged.show_tv.domian.use_case.get_tv_show_latest.GetTvShowLatestFailure
import com.araged.show_tv.domian.use_case.get_tv_show_latest.GetTvShowLatestResponse
import com.araged.show_tv.domian.use_case.get_tv_show_latest.GetTvShowLatestUseCase
import com.araged.show_tv.domian.use_case.get_tv_show_popular.GetTvShowPopularResponse
import com.araged.show_tv.domian.use_case.get_tv_show_rated.GetTvShowRatedResponse
import com.araged.show_tv.domian.use_case.get_tv_show_rated.GetTvShowRatedUseCase
import com.argaed.entities.tv.ShowTv
import com.argaed.entities.tv.ShowTvDetails
import com.example.di.show_tv.ShowTvModule
import dagger.Module
import dagger.Provides
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import javax.inject.Singleton


@Module
@TestInstallIn(
    components = [SingletonComponent::class],
    replaces = [ShowTvModule::class],

    )
class FakeShowTvRepositoryModule {

    @Provides
    @Singleton
    fun provideTvSowRepository(): TvShowRepository = object : TvShowRepository {

        val response = listOf(
            ShowTv(
                backdropPath = "/irwQcdjwtjLnaA0iErabab9PrmG.jpg",
                firstAirDate = "",
                id = 646389,
                name = "",
                originalLanguage = "",
                originalName = "",
                overview = "",
                popularity = 0.0,
                posterPath = "",
                voteAverage = 0.0,
                voteCount = 0,
                category = ""
            )
        )

        override suspend fun getTvShowPopular():
                Either<GetTvShowPopularFailure, GetTvShowPopularResponse> = try {

            Either.Right(GetTvShowPopularResponse(response))
        } catch (e: Exception) {
            Either.Left(GetTvShowPopularFailure.UserTimeOut)
        }

        override suspend fun getTvShowRated():
                Either<GetTvShowRatedFailure, GetTvShowRatedResponse> = try {
            Either.Right(GetTvShowRatedResponse(response))
        } catch (e: Exception) {
            Either.Left(GetTvShowRatedFailure.UserTimeOut)
        }

        override suspend fun getMovieLatest(): Either<GetTvShowLatestFailure, GetTvShowLatestResponse> =
            try {

                Either.Right(GetTvShowLatestResponse(response))
            } catch (e: Exception) {

                Either.Left(GetTvShowLatestFailure.UserDisabled)
            }

        override suspend fun getTvShowDetails(movieID: Int):
                Either<GetTvShowDetailsFailure, GetTvShowDetailsResponse> = try {
            val details= ShowTvDetails(
                backdropPath = "",
                firstAirDate = "",
                homepage = "",
                id = 0,
                inProduction = false,
                lastAirDate = "",
                name = "",
                numberOfEpisodes = 0,
                numberOfSeasons = 0,
                originCountry = listOf(),
                originalLanguage = "",
                originalName = "",
                overview = "",
                popularity = 0.0,
                posterPath = "",
                status = "",
                tagline = "",
                type = "",
                voteAverage = 0.0,
                voteCount = 0
            )
            Either.Right(GetTvShowDetailsResponse(details))
        } catch (e: Exception) {
            Either.Left(GetTvShowDetailsFailure.UserTimeOut)
        }
    }


    @Provides
    fun provideGetMovieRatedUseCase(
        tvShowRepository: TvShowRepository
    ) = GetTvShowRatedUseCase(tvShowRepository)

    @Provides
    fun provideGetMovieUpcomingUseCase(
        tvShowRepository: TvShowRepository
    ) = GetTvShowLatestUseCase(tvShowRepository)

    @Provides
    fun provideGetMovieDetailsUseCase(
        tvShowRepository: TvShowRepository
    ) = GetTvShowDetailsUseCase(tvShowRepository)

}