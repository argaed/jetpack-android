
plugins {
    id ("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("kotlin-parcelize")
    id("kotlin-kapt")
    id("com.google.dagger.hilt.android")
}

android {
    compileSdk = Versions.COMPILE_SDK_VERSION
    buildToolsVersion = Versions.BUILD_TOOLS_VERSION

    defaultConfig {
        minSdk = Versions.MIN_SDK_VERSION
        targetSdk = Versions.TARGET_SDK_VERSION
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }

    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }


}

dependencies {
    /** KOTLIN */
    /** ANDROID X */
    implementation(AndroidX.CORE)
    implementation(AndroidX.APP_COMPAT)
    implementation(AndroidX.ACTIVITY)
    implementation (AndroidXConstrainLayout.CONSTRAINT_LAYOUT)
    /** GOOGLE */
    implementation(Google.AR_CORE)
    /** COMPOSE */
    implementation(AndroidXLifecycle.LIFECYCLE_VIEWMODEL_KTX)
    implementation(AndroidCompose.ANDROID_COMPOSE_UI)
    implementation(AndroidCompose.ANDROID_COMPOSE_COMPILER)
    implementation(AndroidCompose.ANDROID_COMPOSE_MATERIAL)
    implementation(AndroidCompose.ACTIVITY_COMPOSE)
    implementation(AndroidCompose.ANDROID_COMPOSE_TOOLING)
    implementation(AndroidCompose.ANDROID_COMPOSE_RUNTIME)
    implementation(AndroidCompose.ANDROID_COMPOSE_LIVEDATA)
    implementation(AndroidCompose.ANDROID_COMPOSE_UI_UNIT)
    implementation(AndroidCompose.ANDROID_COMPOSE_ANIMATION)
    implementation(AndroidCompose.CONSTRAINT_COMPOSE)
    implementation(AndroidCompose.VIEW_MODEL_COMPOSE)
    implementation(AndroidCompose.ANDROID_COMPOSE_NAVIGATE)
    implementation(AndroidX.WINDOW)
    implementation(AndroidXNavigation.FRAGMENT_KTX)
    implementation(AndroidXNavigation.UI_KTX)

    implementation("com.google.accompanist:accompanist-systemuicontroller:0.21.2-beta")

    /** COROUTINES */
    implementation(Coroutines.CORE)
    implementation(Coroutines.ANDROID)
    implementation(Coroutines.TEST)
    /** GLIDE*/
    implementation(Glide.LAND_SCAPIST)

    /**  DAGGER*/
    androidTestImplementation(Hilt.HILT_TEST)
    implementation(Hilt.DAGGER_ANDROID)
    implementation(Hilt.HILT_NAVIGATE)
    kapt(Hilt.HILT_COMPILER)
    androidTestImplementation(Hilt.HILT_TEST)
    kaptAndroidTest(Hilt.HILT_COMPILER)
    /** MODULE */
    implementation(project(SharedPool.HTTP_CLIENT))
    implementation(project(Core.CLEAN))
    implementation(project(SharedPool.NETWORK))
    implementation(project(Shared.MOVIE))
    implementation(project(Shared.TV_SHOW))
    implementation(project(FeaturePool.RESOURCES))

    /** ROOM */
    implementation(Room.RUNTIME)
    implementation(Room.KTX)
    implementation(Room.COROUTINES)
    kapt(Room.KAPT_COMPILER)
    /** DEBUG */
    implementation(Debug.TIMBER)
    debugImplementation(AndroidCompose.ANDROID_COMPOSE_DEBUG)
    /** GOOGLE */
    implementation(Google.MATERIAL)
    /** COROUTINES **/
    implementation(Coroutines.ANDROID)
    /** TESTING */
    testImplementation(Junit.JUNIT)
    androidTestImplementation(AndroidXTesting.JUNIT)
    androidTestImplementation(AndroidXTesting.ESPRESSO_CORE)
    androidTestImplementation(AndroidCompose.ANDROID_COMPOSE_TEST)

}