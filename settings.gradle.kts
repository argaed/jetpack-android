/* PROJECT */
pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}

include(":app")
rootProject.name = "Jetpack"
include(":sharedPool:httpclient")
include(":core:clean")
include(":core:failureHandler")
include(":sharedPool:network")
include(":shared:movie")
include(":feature:splash")
include(":featurePool:resources")
include(":shared:show_tv")
include("EnterpriseBusinessRules:Entities")
include(":InterfaceAdapter:DataBase")
include(":InterfaceAdapter:DI")
